//
// Created by Nikolay Dmitriev on 12/1/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "UINavigationBar+PMTUINavigationBarCategory.h"
#import "PMTUIColorCategory.h"

@implementation UINavigationBar (PMTUINavigationBarCategory)

- (void)setPhotoPreviewStyle {
    [self setBackgroundImage:[UIColor imageWithColor:[UIColor darkGrayColor] alpha:0.8] forBarMetrics:UIBarMetricsDefault];
    [self setShadowImage:[UIImage new]];
    [self setTintColor:[UIColor whiteColor]];
    [self setTranslucent:YES];
}

- (void)setDefaultStyle {
    [self setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self setTitleTextAttributes:nil];
    [self setBarTintColor:nil];
}

@end