//
// Created by Nikolay Dmitriev on 12/1/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface UINavigationBar (PMTUINavigationBarCategory)

- (void)setPhotoPreviewStyle;

- (void)setDefaultStyle;

@end