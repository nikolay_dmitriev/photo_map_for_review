//
//  PMTPhotoMapAnnotationCalloutView.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 12/4/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <ParseUI/PFImageView.h>
#import "PMTPhotoMapAnnotationCalloutView.h"
#import "PMTCoreDataPhoto.h"
#import "PMTPhotoImageView.h"
#import "PMTCoreDataPhotoData.h"

@interface PMTPhotoMapAnnotationCalloutView ()

@property(weak, nonatomic) IBOutlet PMTPhotoImageView *photoImageView;
@property(weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property(weak, nonatomic) IBOutlet UILabel *dateLabel;

@property(strong, nonatomic) PMTCoreDataPhoto *photo;

@end

@implementation PMTPhotoMapAnnotationCalloutView

#pragma mark - Public

- (void)setupWithPhoto:(PMTCoreDataPhoto *)photo {
    self.photo = photo;

    [self updateLabels];
    [self startLoadingImage];
}

#pragma mark - Actions

- (IBAction)annotationTapped:(id)sender {
    self.photoAnnotationCalloutTapped(self.photo);
}

#pragma mark - Private

//For callout view to be tappable from map
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView != nil) {
        [self.superview bringSubviewToFront:self];
    }
    return hitView;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect rect = self.bounds;
    BOOL isInside = CGRectContainsPoint(rect, point);
    if (!isInside) {
        for (UIView *view in self.subviews) {
            isInside = CGRectContainsPoint(view.frame, point);
            if (isInside)
                break;
        }
    }
    return isInside;
}

- (void)updateLabels {
    self.descriptionLabel.text = self.photo.photoDescription;

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"mm-dd-yy"];
    self.dateLabel.text = [dateFormatter stringFromDate:self.photo.date];
}

- (void)startLoadingImage {
    self.photoImageView.image = [UIImage imageWithData:self.photo.data.photoData];
    // [self.photoImageView setImageFromParseFile:self.photo.uploadedPhoto];
}

@end
