//
//  PMTFullPhotoPreviewFooterView.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/24/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMTCoreDataPhoto;

@interface PMTFullPhotoPreviewFooterView : UIView

- (void)updateWithPhoto:(PMTCoreDataPhoto *)photo;

@end
