//
// Created by Nikolay Dmitriev on 12/10/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoImageView.h"
#import "PMTImageCache.h"

@interface PMTPhotoImageView ()

@property(weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@property(weak, nonatomic) NSURLSessionTask *downloadTask;
@property(strong, nonatomic) NSString *url;

@end

@implementation PMTPhotoImageView

#pragma mark - Lifecycle

- (void)dealloc {
    [self.downloadTask cancel];
}

#pragma mark - Custom Accessors

- (UIActivityIndicatorView *)activityIndicator {
    if (!_activityIndicator) {
        UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc]
                initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        [self addSubview:activityIndicatorView];

        activityIndicatorView.center = CGPointMake(self.bounds.size.width * 0.5f, self.bounds.size.height * 0.5f);

        self.activityIndicator = activityIndicatorView;
    }
    return _activityIndicator;
}

#pragma mark - Public
//If we will clicking back and forward, we can explode

//File should not be replaced while loading
- (void)setImageFromParseFile:(PFFile *)file {
    if (!file || !file.url) {
        return;
    }
    self.url = file.url;
    [self.downloadTask cancel];

    UIImage *cachedImage = [[PMTImageCache sharedInstance] tryGetImageFromCacheWithKey:file.url];

    if (cachedImage) {
        self.image = cachedImage;
        return;
    }

    [self.activityIndicator startAnimating];
    self.image = nil;

    NSURL *url = [NSURL URLWithString:file.url];

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];

    __weak PMTPhotoImageView *weakSelf = self;

    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            return;
        }

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [UIImage imageWithData:data];
            if (!image) {
                return;
            }

            [[PMTImageCache sharedInstance] addImageWithKey:weakSelf.url image:image];

            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.activityIndicator stopAnimating];
                weakSelf.image = image;
            });
        });
    }];

    [task resume];
}


@end