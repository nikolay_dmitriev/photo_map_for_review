//
//  PMTPhotoMapAnnotationCalloutView.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 12/4/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class PMTCoreDataPhoto;

typedef void (^PhotoAnnotationCalloutTappedForPhoto)(PMTCoreDataPhoto *);

@interface PMTPhotoMapAnnotationCalloutView : MKAnnotationView

@property(copy, nonatomic) PhotoAnnotationCalloutTappedForPhoto photoAnnotationCalloutTapped;

- (void)setupWithPhoto:(PMTCoreDataPhoto *)photo;

@end
