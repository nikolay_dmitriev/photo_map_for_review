//
// Created by Nikolay Dmitriev on 12/10/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PFFile.h"

@interface PMTPhotoImageView : UIImageView

- (void)setImageFromParseFile:(PFFile *)file;

@end