//
//  PMTFullPhotoPreviewFooterView.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/24/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTFullPhotoPreviewFooterView.h"
#import "PMTCoreDataPhoto.h"

@interface PMTFullPhotoPreviewFooterView()

@property(weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property(weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation PMTFullPhotoPreviewFooterView

#pragma mark - Public

- (void)updateWithPhoto:(PMTCoreDataPhoto *)photo {
    self.descriptionLabel.text = photo.photoDescription;

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setDateFormat:@"MMMM dd', 'yyyy 'at' HH:mm:ss"];

    self.dateLabel.text = [dateFormatter stringFromDate:photo.date];
}

@end