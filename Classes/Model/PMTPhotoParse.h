//
// Created by Nikolay Dmitriev on 1/10/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "PMTPhotoCategories.h"

@class PMTCoreDataPhoto;

@interface PMTPhotoParse : PFObject <PFSubclassing>

@property(copy, nonatomic) NSString *photoDescription;
@property(strong, nonatomic) PFGeoPoint *location;
@property(strong, nonatomic) NSDate *date;
@property(strong, nonatomic) PFUser *user;
@property(assign, nonatomic) NSInteger photoCategory;

@property(strong, nonatomic) PFFile *uploadedPhoto;

+ (instancetype)parsePhotoWithImage:(UIImage *)image;

+ (instancetype)parsePhotoWithCoreDataPhoto:(PMTCoreDataPhoto *)coreDataPhoto;

+ (NSString *)parseClassName;

- (void)updateWithCoreDataPhoto:(PMTCoreDataPhoto *)coreDataPhoto;

- (void)setupWithImage:(UIImage *)image;

@end
