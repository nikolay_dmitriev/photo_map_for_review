//
// Created by Nikolay Dmitriev on 1/10/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "PMTPhotoParse.h"
#import "PMTCoreDataPhoto.h"
#import "PMTCoreDataPhotoData+CoreDataProperties.h"

@implementation PMTPhotoParse

@dynamic photoDescription;
@dynamic location;
@dynamic date;
@dynamic user;
@dynamic uploadedPhoto;
@dynamic photoCategory;

+ (NSString *)parseClassName {
    return @"Photo";
}

+ (void)load {
    [self registerSubclass];
}

+ (instancetype)parsePhotoWithImage:(UIImage *)image {
    PMTPhotoParse *photoParse = [PMTPhotoParse object];
    [photoParse setupWithImage:image];
    return photoParse;
}

+ (instancetype)parsePhotoWithCoreDataPhoto:(PMTCoreDataPhoto *)coreDataPhoto {
    PMTPhotoParse *photoParse = [PMTPhotoParse object];

    photoParse.photoDescription = coreDataPhoto.photoDescription;
    photoParse.date = coreDataPhoto.date;
    photoParse.photoCategory = [coreDataPhoto.category integerValue];
    photoParse.user = [PFUser currentUser];

    photoParse.location = [PFGeoPoint geoPointWithLatitude:[coreDataPhoto.locationLatitude doubleValue]
                                                 longitude:[coreDataPhoto.locationLongitude doubleValue]];

    photoParse.uploadedPhoto = [PFFile fileWithData:coreDataPhoto.data.photoData];

    return photoParse;
}

- (void)setupWithImage:(UIImage *)image {
    self.photoDescription = @"";
    self.date = [NSDate new];
    self.location = nil;
    self.photoCategory = PhotoCategoryTypeDefault;

    self.uploadedPhoto = [PFFile fileWithData:UIImageJPEGRepresentation(image, 1)];

    self.user = [PFUser currentUser];
}

- (void)updateWithCoreDataPhoto:(PMTCoreDataPhoto *)coreDataPhoto{
    self.photoDescription = coreDataPhoto.photoDescription;
    self.photoCategory = [coreDataPhoto.category integerValue];
}

@end
