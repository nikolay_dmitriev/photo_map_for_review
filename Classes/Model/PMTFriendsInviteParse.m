//
// Created by mac-184 on 1/12/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTFriendsInviteParse.h"


@implementation PMTFriendsInviteParse

@dynamic inviteOwner;
@dynamic invitedUsers;
@dynamic inviteForPhoto;

+ (NSString *)parseClassName {
    return @"FriendsInvite";
}

+ (void)load {
    [self registerSubclass];
}

@end