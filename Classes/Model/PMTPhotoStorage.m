//
// Created by Nikolay Dmitriev on 11/28/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoStorage.h"
#import "PMTPhotoCategory.h"
#import "PMTCoreDataPhoto.h"
#import "PMTPhotoCategories.h"

@interface PMTPhotoStorage ()

//Keep to make fast search and filtering
@property(strong, nonatomic) NSMutableDictionary *photosByDate;
@property(strong, nonatomic) NSMutableDictionary *dateByIndex;

@end

@implementation PMTPhotoStorage

#pragma mark - Custom Accessors

- (NSMutableDictionary *)photosByDate {
    if (!_photosByDate)_photosByDate = [[NSMutableDictionary alloc] init];
    return _photosByDate;
}

- (NSMutableDictionary *)dateByIndex {
    if (!_dateByIndex)_dateByIndex = [[NSMutableDictionary alloc] init];
    return _dateByIndex;
}

#pragma mark - Public

- (instancetype)photoStorageFilteredWithActiveCategories {
    NSArray *filteredPhotos = self.allPhotosInStorage;

    NSPredicate *predicateByCategory = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        PMTCoreDataPhoto *photo = evaluatedObject;
        PMTPhotoCategory *photoCategory = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) photo.category.intValue];
        return photoCategory.isActive;
    }];
    filteredPhotos = [filteredPhotos filteredArrayUsingPredicate:predicateByCategory];

    return [[PMTPhotoStorage alloc] initWithPhotoArray:filteredPhotos];
}

- (instancetype)photoStorageFilteredWithSearchString:(NSString *)searchString {
    NSArray *filteredPhotos = self.allPhotosInStorage;

    NSPredicate *predicateByCategory = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        PMTCoreDataPhoto *photo = evaluatedObject;
        return [photo.photoDescription containsString:searchString];
    }];
    filteredPhotos = [filteredPhotos filteredArrayUsingPredicate:predicateByCategory];

    return [[PMTPhotoStorage alloc] initWithPhotoArray:filteredPhotos];
}

- (instancetype)initWithPhotoArray:(NSArray *)photos {
    //Golden path to avoid code indention
    if (self != [super init]) {
        return self;
    }

    _allPhotosInStorage = [self sortPhotosByDate:photos];

    for (PMTCoreDataPhoto *photo in self.allPhotosInStorage) {
        //Unique key like 12-15, 11-18, 01-90 mm-yy
        NSString *dateKey = [self extractKeyFromDate:photo.date];

        NSMutableArray *photosInThisMonth = self.photosByDate[dateKey];
        //As array is already sorted by date, if there are no key, then we can increase date index and add ne date key
        if (!photosInThisMonth) {
            photosInThisMonth = [NSMutableArray new];
            self.photosByDate[dateKey] = photosInThisMonth;

            NSInteger newDateIndex = [self.dateByIndex count];
            self.dateByIndex[@(newDateIndex)] = dateKey;
        }
        [photosInThisMonth addObject:photo];
    }
    return self;
}

- (NSUInteger)totalMonthInStorage {
    return [self.dateByIndex count];
}

- (NSArray *)photosForMonthWithIndex:(NSUInteger)index {
    NSString *dateKey = self.dateByIndex[@(index)];
    return self.photosByDate[dateKey];
}

#pragma mark - Private

- (NSString *)extractKeyFromDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM:yy"];
    return [dateFormatter stringFromDate:date];
}

- (NSArray *)sortPhotosByDate:(NSArray *)photos {
    return [photos sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate *firstDate = ((PMTCoreDataPhoto *) obj1).date;
        NSDate *secondDate = ((PMTCoreDataPhoto *) obj2).date;
        return [secondDate compare:firstDate];
    }];
}

@end