//
// Created by Nikolay Dmitriev on 1/10/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CoreDataSynchronizationCompleted)();

@interface PMTCoreDataParseSynchronization : NSObject

- (void)synchronize;
@property(copy, nonatomic) CoreDataSynchronizationCompleted onCoreDataSynchronizationCompleted;

@end