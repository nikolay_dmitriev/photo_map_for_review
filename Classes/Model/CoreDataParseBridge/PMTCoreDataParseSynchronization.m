//
// Created by Nikolay Dmitriev on 1/10/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTCoreDataParseSynchronization.h"
#import "PMTCoreDataUser.h"
#import "PMTCoreDataStorage.h"
#import "Parse/Parse.h"
#import "PMTViewUtilities.h"
#import "PMTPhotoParse.h"
#import "PMTCoreDataPhoto+CoreDataProperties.h"
#import "PMTOperationQueue.h"

@interface PMTCoreDataParseSynchronization ()

@property(assign, nonatomic) BOOL isSyncInProgress;

@property(strong, nonatomic) NSArray *parsePhotosToSync;
@property(strong, nonatomic) NSMutableArray *coreDataPhotosToSync;

@property(strong, nonatomic) NSMutableArray *parsePhotosToDownload;
@property(strong, nonatomic) NSMutableArray *coreDataPhotosToUpload;

@end

@implementation PMTCoreDataParseSynchronization

#pragma mark - Public

- (NSDate *)lastSynchronizationDate {
    PMTCoreDataUser *user = [[PMTCoreDataStorage sharedInstance] currentUser];
    return user.lastSynchronizationWithParse;
}

- (void)onSynchronizationTick {
    if (self.isSyncInProgress) {
        return;
    }
    NSLog(@"Synchronization Started");
    self.isSyncInProgress = YES;

    NSDate *lastSyncDate = [self lastSynchronizationDate];
    [self receiveParsePhotosWithUpdateAfterDate:lastSyncDate];
}

- (void)synchronize {
    [self onSynchronizationTick];
}

#pragma mark - Custom Accessors

- (NSArray *)parsePhotosToSync {
    if (!_parsePhotosToSync) _parsePhotosToSync = [NSArray new];
    return _parsePhotosToSync;
}

- (NSMutableArray *)parsePhotosToDownload {
    if (!_parsePhotosToDownload) _parsePhotosToDownload = [NSMutableArray new];
    return _parsePhotosToDownload;
}

- (NSMutableArray *)coreDataPhotosToSync {
    if (!_coreDataPhotosToSync) _coreDataPhotosToSync = [NSMutableArray new];
    return _coreDataPhotosToSync;
}

- (NSMutableArray *)coreDataPhotosToUpload {
    if (!_coreDataPhotosToUpload) _coreDataPhotosToUpload = [NSMutableArray new];
    return _coreDataPhotosToUpload;
}

#pragma mark - Private

- (void)synchronizeParsePhotosWithCoreData {
    NSLog(@"Synchronizing photos from Parse with local CoreData photos");
    [self.parsePhotosToSync enumerateObjectsUsingBlock:^(PMTPhotoParse *parsePhoto, NSUInteger idx, BOOL *stop) {
        PMTCoreDataPhoto *coreDataPhoto = [self coreDataPhotoWithId:parsePhoto.objectId];
        if (!coreDataPhoto) {
            NSLog(@"Can't map Parse photo with id %@ to CoreData, adding photo to download queue", parsePhoto.objectId);
            [self.parsePhotosToDownload addObject:parsePhoto];
        } else {
            if ([coreDataPhoto.updatedAt compare:parsePhoto.updatedAt] == NSOrderedDescending) {
                NSLog(@"Local photo with id %@ has newer update date", parsePhoto.objectId);
                [parsePhoto updateWithCoreDataPhoto:coreDataPhoto];
                [parsePhoto saveInBackground];
            } else {
                NSLog(@"Parse photo with id %@ has newer update date", parsePhoto.objectId);
                [coreDataPhoto updateWithParsePhoto:parsePhoto];
            }
        }
    }];

    NSLog(@"Parse photos synchronized successfully");

    [[PMTCoreDataStorage sharedInstance] saveContext];
    self.parsePhotosToSync = nil;

    [self prepareCoreDataForSynchronization];
    [self downloadPhotosFromParse];
}

- (void)synchronizeCoreDataPhotosWithParse {
    PMTCoreDataPhoto *coreDataPhotoToSync = [self.coreDataPhotosToSync lastObject];
    NSLog(@"Synchronizing CoreData photos with Parse. Photos to synchronize : %u", [self.coreDataPhotosToSync count]);
    if (!coreDataPhotoToSync) {
        NSLog(@"All Core Data changes synchronized");
        [self uploadPhotosToParse];
        return;
    }
    __weak PMTCoreDataParseSynchronization *weakSelf = self;
    PFQuery *query = [PFQuery queryWithClassName:[PMTPhotoParse parseClassName]];
    [query getObjectInBackgroundWithId:coreDataPhotoToSync.parseId block:^(PFObject *photo, NSError *error) {
        if (!error) {
            PMTPhotoParse *parsePhoto = (PMTPhotoParse *) photo;
            PMTCoreDataPhoto *coreDataPhoto = [weakSelf.coreDataPhotosToSync lastObject];
            [weakSelf.coreDataPhotosToSync removeLastObject];
            if ([coreDataPhoto.updatedAt compare:parsePhoto.updatedAt] == NSOrderedDescending) {
                [parsePhoto updateWithCoreDataPhoto:coreDataPhoto];
                [parsePhoto saveInBackground];
                NSLog(@"Local photo with id %@ has newer update date", parsePhoto.objectId);
            } else {
                [coreDataPhoto updateWithParsePhoto:parsePhoto];
                NSLog(@"Parse photo with id %@ has newer update date", parsePhoto.objectId);
            }
            [weakSelf synchronizeCoreDataPhotosWithParse];
        }
    }];
}

- (void)downloadPhotosFromParse {
    NSLog(@"Need to download %lu photos from Parse", (unsigned long) [self.parsePhotosToDownload count]);

    PMTPhotoParse *photoToDownload = [self.parsePhotosToDownload lastObject];

    if (!photoToDownload) {
        NSLog(@"Parse photos downloaded successfully");
        [[PMTCoreDataStorage sharedInstance] saveContext];
        [self synchronizeCoreDataPhotosWithParse];
        return;
    }

    __weak PMTCoreDataParseSynchronization *weakSelf = self;
    [photoToDownload.uploadedPhoto getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            NSLog(@"Downloaded Parse photo with id %@", photoToDownload.objectId);
            [[PMTCoreDataStorage sharedInstance] saveParsePhoto:photoToDownload data:data];
            [weakSelf.parsePhotosToDownload removeLastObject];
            [weakSelf downloadPhotosFromParse];
        }
    }];
}

- (void)uploadPhotosToParse {
    NSLog(@"Need to upload %lu photos to Parse", (unsigned long) [self.coreDataPhotosToUpload count]);

    PMTCoreDataPhoto *photoToUpload = [self.coreDataPhotosToUpload lastObject];

    if (!photoToUpload) {
        NSLog(@"CoreData photos uploaded to Parse successfully");
        [[PMTCoreDataStorage sharedInstance] saveContext];
        [self onSynchronizationEnd];
        return;
    }

    PMTPhotoParse *parsePhoto = [PMTPhotoParse parsePhotoWithCoreDataPhoto:photoToUpload];

    __weak PMTCoreDataParseSynchronization *weakSelf = self;
    [parsePhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            PMTCoreDataPhoto *uploadedCoreDataPhoto = [weakSelf.coreDataPhotosToUpload lastObject];
            uploadedCoreDataPhoto.parseId = parsePhoto.objectId;
            NSLog(@"Uploaded CoreData photo to Parse. Parse id : %@", parsePhoto.objectId);
            [weakSelf.coreDataPhotosToUpload removeLastObject];
            [weakSelf uploadPhotosToParse];
        }
    }];
}

- (void)onSynchronizationEnd {
    NSLog(@"Synchronization completed successfully");
    self.isSyncInProgress = NO;
    [[PMTCoreDataStorage sharedInstance] currentUser].lastSynchronizationWithParse = [NSDate new];
    [[PMTCoreDataStorage sharedInstance] saveContext];

    self.onCoreDataSynchronizationCompleted();
}

#pragma mark - Parse Private

- (void)receiveParsePhotosWithUpdateAfterDate:(NSDate *)date {
    PFQuery *allUserPhotosQuery = [PFQuery queryWithClassName:[PMTPhotoParse parseClassName]];
    [allUserPhotosQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    [allUserPhotosQuery addDescendingOrder:@"date"];
    //ToDo : Possible date collision here, when in parse we store photo with the same date
    if (date) {
        [allUserPhotosQuery whereKey:@"updatedAt" greaterThan:date];
    }
    allUserPhotosQuery.limit = 1000;

    NSLog(@"Requesting photos from Parse with update date later then : %@", date);

    __weak PMTCoreDataParseSynchronization *weakSelf = self;
    [allUserPhotosQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            [weakSelf onParsePhotosReceived:objects];
        } else {
            [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Error"
                                                             withText:error.description];
        }
    }];
}

- (void)onParsePhotosReceived:(NSArray *)photos {
    NSLog(@"%@ : %lu", @"Received photos from Parse", (unsigned long) [photos count]);

    self.parsePhotosToSync = photos;
    [self synchronizeParsePhotosWithCoreData];
}

#pragma mark - CoreData Private

- (NSArray *)localPhotosNeedsToUpdate {
    NSArray *allLocalPhotos = [[[PMTCoreDataStorage sharedInstance] currentUser].photos allObjects];
    NSDate *lastSynchronizationDate = [self lastSynchronizationDate];
    if (!lastSynchronizationDate) {
        return [allLocalPhotos mutableCopy];
    }

    NSPredicate *predicateByUpdateDate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        PMTCoreDataPhoto *photo = evaluatedObject;
        return [photo.updatedAt compare:lastSynchronizationDate] == NSOrderedDescending;
    }];

    NSMutableArray *photosToUpdate = [[allLocalPhotos filteredArrayUsingPredicate:predicateByUpdateDate] mutableCopy];
    return photosToUpdate;
}

- (PMTCoreDataPhoto *)coreDataPhotoWithId:(NSString *)parseObjectId {
    NSPredicate *predicateByUpdateDate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        PMTCoreDataPhoto *photo = evaluatedObject;
        return [photo.parseId isEqualToString:parseObjectId];
    }];
    //ToDo : performance bottleneck
    return [[[[[[PMTCoreDataStorage sharedInstance] currentUser] photos] allObjects] filteredArrayUsingPredicate:predicateByUpdateDate] lastObject];
}

- (void)prepareCoreDataForSynchronization {
    NSArray *photosNeedToUpdate = [self localPhotosNeedsToUpdate];

    NSPredicate *predicateByEmptyId = [NSPredicate predicateWithBlock:^BOOL(PMTCoreDataPhoto *photo, NSDictionary *bindings) {
        return photo.parseId == nil;
    }];

    NSPredicate *predicateWithFullId = [NSPredicate predicateWithBlock:^BOOL(PMTCoreDataPhoto *photo, NSDictionary *bindings) {
        return photo.parseId != nil;
    }];

    self.coreDataPhotosToUpload = [[photosNeedToUpdate filteredArrayUsingPredicate:predicateByEmptyId] mutableCopy];
    NSLog(@"%@ : %lu", @"CoreData photos to upload to Parse ", (unsigned long) [self.coreDataPhotosToUpload count]);
    self.coreDataPhotosToSync = [[photosNeedToUpdate filteredArrayUsingPredicate:predicateWithFullId] mutableCopy];
    NSLog(@"%@ : %lu", @"CoreData photos to synchronize with Parse ", (unsigned long) [self.coreDataPhotosToSync count]);
}
@end