//
// Created by mac-184 on 1/12/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface PMTFriendsInviteParse : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

@property (strong, nonatomic) PFUser *inviteOwner; //To show invite from User to others
@property (strong, nonatomic) NSArray *invitedUsers; //For now it's only client implementation
@property (strong, nonatomic) NSString *inviteForPhoto; //ToDo : Add permissions here, if implement invites by Parse

@end