//
// Created by mac-184 on 1/15/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTInviteFriendsViewModel.h"
#import "UIKit/UIKit.h"
#import "PMTUserContactsStorage.h"

@interface PMTInviteFriendsViewModel ()

@property(strong, nonatomic) PMTUserContactsStorage *contactStorage;

@property(strong, nonatomic) NSArray *allContacts;
@property(strong, nonatomic) NSMutableDictionary *splittedByFirstLetterContacts;

@property (strong, nonatomic) NSMutableDictionary *indexToLetterStorage;

@end

@implementation PMTInviteFriendsViewModel

#pragma mark - Custom Accessors

- (NSMutableDictionary *)splittedByFirstLetterContacts {
    if (!_splittedByFirstLetterContacts) _splittedByFirstLetterContacts = [NSMutableDictionary new];
    return _splittedByFirstLetterContacts;
}

- (NSArray *)allContacts {
    if (!_allContacts) _allContacts = [NSMutableArray new];
    return _allContacts;
}

- (PMTUserContactsStorage *)contactStorage {
    if (!_contactStorage)_contactStorage = [PMTUserContactsStorage new];
    return _contactStorage;
}

- (NSMutableDictionary *)indexToLetterStorage {
    if(!_indexToLetterStorage) _indexToLetterStorage = [NSMutableDictionary new];
    return _indexToLetterStorage;
}

#pragma mark - Public

- (void)fetchContactsInBackgroundWithBlock:(InviteFriendsViewModelDataReceived)block {
    [self.contactStorage fetchContactsWithBlock:^(NSArray *contacts) {
        self.allContacts = contacts;
        __weak PMTInviteFriendsViewModel *weakSelf = self;
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"firstname" ascending:YES];
        contacts = [contacts sortedArrayUsingDescriptors:@[sort]];
        [contacts enumerateObjectsUsingBlock:^(PMTUserContact *singleContact, NSUInteger idx, BOOL *stop) {
            [weakSelf addContact:singleContact];
        }];
        block();
    }];
}

- (void)addContact:(PMTUserContact *)contact {
    NSString *key = [contact.firstname substringToIndex:1];
    if (!self.splittedByFirstLetterContacts[key]) {
        self.splittedByFirstLetterContacts[key] = [NSMutableArray new];

        int keyMappingIndex = [self.splittedByFirstLetterContacts count];
        self.indexToLetterStorage[@(keyMappingIndex)] = key;
    }
    [self.splittedByFirstLetterContacts[key] addObject:contact];
}

- (NSString *)sectionHeaderForIndex:(NSInteger)index {
    NSString *key = self.indexToLetterStorage[@(index)];
    return key;
}

- (PMTUserContact *)contactForIndex:(NSIndexPath *)indexPath {
    NSString *key = self.indexToLetterStorage[@(indexPath.section)];
    NSMutableArray *array = self.splittedByFirstLetterContacts[key];
    return array[(NSUInteger) indexPath.row];
}

- (NSInteger)numberOfSectionsInTable {
    return [self.indexToLetterStorage count];
}

- (NSInteger)numberOfRowsInSectionWithIndex:(NSInteger)index {
    NSString *key = self.indexToLetterStorage[@(index)];
    return [self.splittedByFirstLetterContacts[key] count];
}

- (NSArray *)invitedContacts {
    NSMutableArray *invitedNames = [NSMutableArray new];
    [self.allContacts enumerateObjectsUsingBlock:^(PMTUserContact *contact, NSUInteger idx, BOOL *stop) {
        if (contact.isInvited) {
            [invitedNames addObject:[NSString stringWithFormat:@"%@ %@", contact.firstname, contact.lastname]];
        }
    }];
    return invitedNames;
}

@end