//
// Created by mac-184 on 1/15/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMTUserContact.h"

typedef void (^InviteFriendsViewModelDataReceived)();

@interface PMTInviteFriendsViewModel : NSObject

- (void)fetchContactsInBackgroundWithBlock:(InviteFriendsViewModelDataReceived)block;

- (NSString *)sectionHeaderForIndex:(NSInteger)index;

- (NSInteger)numberOfSectionsInTable;

- (NSInteger)numberOfRowsInSectionWithIndex:(NSInteger)index;

- (PMTUserContact *)contactForIndex:(NSIndexPath *)indexPath;

- (NSArray *)invitedContacts;

@end