//
// Created by mac-184 on 1/15/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PMTUserContact : NSObject

@property(copy, nonatomic) NSString *firstname;
@property(copy, nonatomic) NSString *lastname;
@property(assign, nonatomic) BOOL isInvited;

@end