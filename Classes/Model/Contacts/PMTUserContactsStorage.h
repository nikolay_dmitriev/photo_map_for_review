//
// Created by mac-184 on 1/15/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ReceivedContactsFromUser)(NSArray *contacts);

@interface PMTUserContactsStorage : NSObject

- (void)fetchContactsWithBlock:(ReceivedContactsFromUser)block;

@end