//
// Created by mac-184 on 1/15/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>
#import "PMTUserContactsStorage.h"
#import "PMTUserContact.h"

@implementation PMTUserContactsStorage

#pragma mark - Public

- (void)fetchContactsWithBlock:(ReceivedContactsFromUser)block {
    [self receiveContactsViaCNContactsWithBlock:block];
}

- (void)receiveContactsViaCNContactsWithBlock:(ReceivedContactsFromUser)block {
    CNContactStore *store = [[CNContactStore alloc] init];

    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError *_Nullable error) {
        if (granted) {

            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *bookError;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&bookError];

            if (bookError) {
                block(nil);
            } else {
                NSMutableArray *result = [NSMutableArray new];

                for (CNContact *contact in cnContacts) {
                    PMTUserContact *userContact = [PMTUserContact new];
                    userContact.firstname = contact.givenName;
                    userContact.lastname = contact.familyName;
                    [result addObject:userContact];
                }

                block(result);
            }
        } else {
            block(nil);
        }
    }];
}

- (void)receiveContactsViaABAddressWithBlock:(ReceivedContactsFromUser)block {
    CFErrorRef error = nil;

    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, &error);

    __block CFArrayRef contactsObjects;

    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                block([self convertABContacts:contactsObjects]);
            } else {
                block(nil);
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        contactsObjects = ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        block([self convertABContacts:contactsObjects]);
    }
    else {
        block(nil);
    }
}

- (NSArray *)convertABContacts:(CFArrayRef)contactsObjects {
    NSMutableArray *result = [NSMutableArray new];

    NSArray *abConstants = (__bridge NSArray *) contactsObjects;
    for (int i = 0; i < [abConstants count]; i++) {
        PMTUserContact *userContact = [PMTUserContact new];
        userContact.firstname = (__bridge_transfer NSString *) ABRecordCopyValue(CFArrayGetValueAtIndex(contactsObjects, i), kABPersonFirstNameProperty);
        userContact.lastname = (__bridge_transfer NSString *) ABRecordCopyValue(CFArrayGetValueAtIndex(contactsObjects, i), kABPersonLastNameProperty);
        [result addObject:userContact];
    }

    return result;
}

@end