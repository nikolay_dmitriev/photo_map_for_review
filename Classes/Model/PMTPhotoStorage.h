//
// Created by Nikolay Dmitriev on 11/28/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMTCoreDataPhoto;


@interface PMTPhotoStorage : NSObject

@property(strong, nonatomic, readonly) NSArray *allPhotosInStorage;

//Creates swallow copy
- (instancetype)photoStorageFilteredWithActiveCategories;

//Creates swallow copy
- (instancetype)photoStorageFilteredWithSearchString:(NSString *)searchString;

- (instancetype)initWithPhotoArray:(NSArray *)photos;

- (NSUInteger)totalMonthInStorage;

- (NSArray *)photosForMonthWithIndex:(NSUInteger)index;

@end