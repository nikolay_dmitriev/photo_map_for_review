//
// Created by mac-184 on 1/8/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UIKit/UIKit.h"

@class PMTCoreDataPhoto;
@class PMTCoreDataUser;
@class PMTPhotoParse;

@interface PMTCoreDataStorage : NSObject

+ (instancetype)sharedInstance;

- (void)drop;

- (void)show;

- (void)saveContext;

- (void)deletePhoto:(PMTCoreDataPhoto *)photo;

- (NSURL *)applicationDocumentsDirectory;

- (PMTCoreDataPhoto *)generateEmptyPhoto;

- (PMTCoreDataPhoto *)generatePhotoWithImage:(UIImage *)image;

- (void)saveParsePhoto:(PMTPhotoParse *)parsePhoto data:(NSData *)data;

- (PMTCoreDataUser *)currentUser;
@end