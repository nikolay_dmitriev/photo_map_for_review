//
// Created by mac-184 on 1/8/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTCoreDataStorage.h"
#import "PMTCoreDataUser.h"
#import "PMTCoreDataPhoto.h"
#import "PMTCoreDataPhotoData.h"
#import "PMTPhotoParse.h"

@interface PMTCoreDataStorage ()

@property(strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property(strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property(strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation PMTCoreDataStorage

#pragma mark - Public

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)saveParsePhoto:(PMTPhotoParse *)parsePhoto data:(NSData *)data{
    [PMTCoreDataPhoto insertNewObjectIntoContext:self.managedObjectContext parsePhoto:parsePhoto data:data];
    [self saveContext];
}

- (void)deletePhoto:(PMTCoreDataPhoto *)photo{
    [self.managedObjectContext deleteObject:photo];
    [self saveContext];
}

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (PMTCoreDataPhoto *)generateEmptyPhoto {
    PMTCoreDataPhotoData *photoData = [PMTCoreDataPhotoData insertNewObjectIntoContext:self.managedObjectContext];
    photoData.photoData = nil;

    PMTCoreDataPhoto *photo = [PMTCoreDataPhoto insertNewObjectIntoContext:self.managedObjectContext];
    photo.user = [self currentUser];
    photo.data = photoData;
    photo.category = @0;
    photo.photoDescription = @"";

    photo.date = [NSDate new];

    return photo;
}

- (PMTCoreDataPhoto *)generatePhotoWithImage:(UIImage *)image {
    PMTCoreDataPhoto *generatedPhoto = [self generateEmptyPhoto];
    generatedPhoto.data.photoData = UIImageJPEGRepresentation(image, 1);
    return generatedPhoto;
}

- (PMTCoreDataUser *)currentUser {
    NSString *currentUserId = [PFUser currentUser].objectId;

    NSFetchRequest *usersResuest = [NSFetchRequest fetchRequestWithEntityName:[PMTCoreDataUser entityName]];
    [usersResuest setPredicate:[NSPredicate predicateWithFormat:@"parseUserObjectId == %@", currentUserId]];

    NSError *error;
    NSArray *results = [self.managedObjectContext executeFetchRequest:usersResuest error:&error];
    if (!results) {
        NSLog(@"Error fetching users objects : %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }

    if(results.count){ //if have user in CoreData
        return [results lastObject];
    }

    PMTCoreDataUser *user = [PMTCoreDataUser insertNewObjectIntoContext:self.managedObjectContext];
    //ToDo : Receive parse user id here
    user.parseUserObjectId = currentUserId;

    return user;
}

- (void)drop {
    NSArray *stores = [self.persistentStoreCoordinator persistentStores];
    for (NSPersistentStore *store in stores) {
        [self.persistentStoreCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
    }
    _managedObjectModel = nil;
    _managedObjectContext = nil;
    _persistentStoreCoordinator = nil;
}

- (void)show {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[PMTCoreDataUser entityName]];

    NSError *error;
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (!results) {
        NSLog(@"Error fetching users objects : %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }

    if (![results count]) {
        NSLog(@"No user objects");
        return;
    }

    for (PMTCoreDataUser *user in results) {
        NSSet *photos = user.photos;
        [photos enumerateObjectsUsingBlock:^(PMTCoreDataPhoto *photo, BOOL *stop) {
            NSLog(@"User id :  %@", photo.user.parseUserObjectId);
            NSLog(@"Photo description : %@", photo.photoDescription);
            NSLog(@"Date :  %@", photo.date);
            NSLog(@"Latitude : %@", photo.locationLatitude);
            NSLog(@"Longitude : %@", photo.locationLongitude);
//            NSLog(@"PhotoData : %@", photo.data.photoData);
        }];
    }
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Custom Accessors

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PMTPhotoStorageLocal" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    // Create the coordinator and store

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TestCoreData.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

@end