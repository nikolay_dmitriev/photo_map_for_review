//
//  PMTCoreDataUser+CoreDataProperties.h
//  PhotoMap
//
//  Created by mac-184 on 1/11/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMTCoreDataUser.h"

@class PMTCoreDataPhoto;

NS_ASSUME_NONNULL_BEGIN

@interface PMTCoreDataUser (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *lastSynchronizationWithParse;
@property (nullable, nonatomic, retain) NSString *parseUserObjectId;
@property (nullable, nonatomic, retain) NSSet<PMTCoreDataPhoto *> *photos;

@end

@interface PMTCoreDataUser (CoreDataGeneratedAccessors)

- (void)addPhotosObject:(PMTCoreDataPhoto *)value;
- (void)removePhotosObject:(PMTCoreDataPhoto *)value;
- (void)addPhotos:(NSSet<PMTCoreDataPhoto *> *)values;
- (void)removePhotos:(NSSet<PMTCoreDataPhoto *> *)values;

@end

NS_ASSUME_NONNULL_END
