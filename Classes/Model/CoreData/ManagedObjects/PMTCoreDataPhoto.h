//
//  PMTCoreDataPhoto.h
//  PhotoMap
//
//  Created by mac-184 on 1/8/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

@class PMTCoreDataUser;
@class PMTPhotoParse;

NS_ASSUME_NONNULL_BEGIN

@interface PMTCoreDataPhoto : NSManagedObject

+ (NSString *)entityName;

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context;

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context parsePhoto:(PMTPhotoParse *)parsePhoto data:(NSData *)data;

- (void)updateWithParsePhoto:(PMTPhotoParse *)parsePhoto;

- (CLLocationCoordinate2D)location;

- (void)setLocation:(CLLocationCoordinate2D)location;

@end

NS_ASSUME_NONNULL_END

#import "PMTCoreDataPhoto+CoreDataProperties.h"
