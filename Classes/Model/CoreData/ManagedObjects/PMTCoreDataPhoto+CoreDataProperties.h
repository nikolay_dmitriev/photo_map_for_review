//
//  PMTCoreDataPhoto+CoreDataProperties.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 1/11/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMTCoreDataPhoto.h"

@class PMTCoreDataPhotoData;

NS_ASSUME_NONNULL_BEGIN

@interface PMTCoreDataPhoto (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *category;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *locationLatitude;
@property (nullable, nonatomic, retain) NSNumber *locationLongitude;
@property (nullable, nonatomic, retain) NSString *photoDescription;
@property (nullable, nonatomic, retain) NSDate *updatedAt;
@property (nullable, nonatomic, retain) NSString *parseId;
@property (nullable, nonatomic, retain) PMTCoreDataPhotoData *data;
@property (nullable, nonatomic, retain) PMTCoreDataUser *user;

@end

NS_ASSUME_NONNULL_END


