//
//  PMTCoreDataUser.m
//  PhotoMap
//
//  Created by mac-184 on 1/8/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTCoreDataUser.h"

@implementation PMTCoreDataUser

+ (NSString *)entityName{
    return @"PMTCoreDataUser";
}

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context{
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityName] inManagedObjectContext:context];
}

@end
