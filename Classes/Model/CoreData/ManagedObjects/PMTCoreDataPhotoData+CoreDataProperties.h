//
//  PMTCoreDataPhotoData+CoreDataProperties.h
//  PhotoMap
//
//  Created by mac-184 on 1/8/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMTCoreDataPhotoData.h"

NS_ASSUME_NONNULL_BEGIN

@interface PMTCoreDataPhotoData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *photoData;
@property (nullable, nonatomic, retain) PMTCoreDataPhoto *photo;

@end

NS_ASSUME_NONNULL_END
