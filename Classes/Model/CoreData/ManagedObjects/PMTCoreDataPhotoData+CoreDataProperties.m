//
//  PMTCoreDataPhotoData+CoreDataProperties.m
//  PhotoMap
//
//  Created by mac-184 on 1/8/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMTCoreDataPhotoData+CoreDataProperties.h"

@implementation PMTCoreDataPhotoData (CoreDataProperties)

@dynamic photoData;
@dynamic photo;

@end
