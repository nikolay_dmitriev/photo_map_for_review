//
//  PMTCoreDataPhoto.m
//  PhotoMap
//
//  Created by mac-184 on 1/8/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "PMTCoreDataPhoto.h"
#import "PMTCoreDataUser.h"
#import "PMTPhotoParse.h"
#import "PMTCoreDataPhotoData+CoreDataProperties.h"
#import "PMTCoreDataStorage.h"

@implementation PMTCoreDataPhoto

+ (NSString *)entityName {
    return @"PMTCoreDataPhoto";
}

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context {
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityName] inManagedObjectContext:context];
}

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context parsePhoto:(PMTPhotoParse *)parsePhoto data:(NSData *)data{
    PMTCoreDataPhoto *photo = [[PMTCoreDataStorage sharedInstance] generateEmptyPhoto];

    photo.photoDescription = parsePhoto.photoDescription;
    photo.date = parsePhoto.date;
    photo.category = @(parsePhoto.photoCategory);
    photo.parseId = parsePhoto.objectId;
    photo.updatedAt = [NSDate new];

    [photo setLocation:CLLocationCoordinate2DMake(parsePhoto.location.latitude, parsePhoto.location.longitude)];

    
    photo.data.photoData = data;
    
    photo.user = [[PMTCoreDataStorage sharedInstance] currentUser];
    
    return photo;
}

- (void)updateWithParsePhoto:(PMTPhotoParse *)parsePhoto {
    self.photoDescription = parsePhoto.photoDescription;
    self.category = @(parsePhoto.photoCategory);
    self.updatedAt = [NSDate new];
}

- (CLLocationCoordinate2D)location {
    CLLocationCoordinate2D result;
    result.latitude = self.locationLatitude.doubleValue;
    result.longitude = self.locationLongitude.doubleValue;
    return result;
}

- (void)setLocation:(CLLocationCoordinate2D)location {
    self.locationLatitude = @(location.latitude);
    self.locationLongitude = @(location.longitude);
}

@end
