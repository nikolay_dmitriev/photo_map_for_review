//
//  PMTCoreDataPhoto+CoreDataProperties.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 1/11/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PMTCoreDataPhoto+CoreDataProperties.h"
#import "PMTCoreDataPhotoData.h"

@implementation PMTCoreDataPhoto (CoreDataProperties)

@dynamic category;
@dynamic date;
@dynamic locationLatitude;
@dynamic locationLongitude;
@dynamic photoDescription;
@dynamic updatedAt;
@dynamic parseId;
@dynamic data;
@dynamic user;

@end
