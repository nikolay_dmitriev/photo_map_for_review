//
//  PMTCoreDataUser.h
//  PhotoMap
//
//  Created by mac-184 on 1/8/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PMTCoreDataUser : NSManagedObject

+ (NSString *)entityName;

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "PMTCoreDataUser+CoreDataProperties.h"
