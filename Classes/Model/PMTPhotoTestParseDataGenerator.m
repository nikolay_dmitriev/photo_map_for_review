//
// Created by Nikolay Dmitriev on 12/10/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoTestParseDataGenerator.h"
#import "PMTCoreDataPhoto.h"
#import "UIKit/UIKit.h"
#import "PMTPhotoCategories.h"
#import "PFGeoPoint.h"


@implementation PMTPhotoTestParseDataGenerator

+ (void)generatePhotos {
//    NSUInteger numberOfPhotosToGenerate = 500;

//    for (int i = 0; i < numberOfPhotosToGenerate; i++) {
//        PMTCoreDataPhoto *photo = [PMTCoreDataPhoto object];
//        [photo setupWithImage:[self randomPhoto]];
//        photo.photoDescription = [self randomDescription];
//        photo.date = [self randomDate];
//        photo.location = [self randomLocation];
//        photo.photoCategory = [self randomCategory];
//        [photo saveInBackground];
//    }
}

+ (NSString *)randomDescription {
    NSArray *descriptionVariants = @[@"My girlfriend", @"New Year",
            @"My best friend shows me his new Mac book", @"Our wedding at Spain",
            @"Crazy weekend", @"Amazing view", @"", @"", @""];

    NSUInteger randomVariantIndex = arc4random() % [descriptionVariants count];
    return descriptionVariants[randomVariantIndex];
}

+ (UIImage *)randomPhoto {
    NSArray *photoName = @[@"IconCamera", @"IconCategories", @"IconCenterOnMe", @"IconMap", @"IconMore",
            @"IconTimeline", @"MarkerDefault", @"MarkerFriends", @"MarkerNature"];
    NSUInteger randomVariantIndex = arc4random() % [photoName count];

    UIImage *result = [UIImage imageNamed:photoName[randomVariantIndex]];

    return result;
}

+ (PhotoCategoriesTypes)randomCategory{
//    int variant = arc4random() % [[PMTPhotoCategories sharedInstance] count];
//    return (PhotoCategoriesTypes) variant;
    return (PhotoCategoriesTypes) 0;
}

+ (PFGeoPoint *)randomLocation {
//    int latitude = (arc4random() % 5) + 30;
//    int longitude = (arc4random() % 20) - 140;
//
//    float latitudePart = (arc4random() % 100)/100.0f;
//    float longitudePart = (arc4random() % 100)/100.0f;
//
//    latitudePart += latitude;
//    longitudePart += longitude;
//
//    PFGeoPoint *result = [PFGeoPoint geoPointWithLatitude:latitudePart longitude:longitudePart];
//    return result;
    return nil;
}

+ (NSDate *)randomDate {
    NSInteger randomDay = arc4random() % 28 + 1;
    NSInteger randomMonth = arc4random() % 12 + 1;
    NSInteger randomYear = arc4random() % 20 + 1990;

    return [self createDate:randomDay month:randomMonth year:randomYear];
}

+ (NSDate *)createDate:(NSInteger)day month:(NSInteger)month year:(NSInteger)year {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:day];
    [comps setMonth:month];
    [comps setYear:year];

    NSCalendar *gregorian = [[NSCalendar alloc]
            initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [gregorian dateFromComponents:comps];

    return date;
}

@end