//
// Created by Nikolay Dmitriev on 12/10/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PMTPhotoTestParseDataGenerator : NSObject

+ (void)generatePhotos;

@end