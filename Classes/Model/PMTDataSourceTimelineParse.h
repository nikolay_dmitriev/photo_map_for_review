//
// Created by Nikolay Dmitriev on 12/14/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMTPhotoStorage;

typedef void(^ParseDataReceivedBlock)();

@interface PMTDataSourceTimelineParse : NSObject

@property(strong, nonatomic) PMTPhotoStorage *photoStorage;

@property(strong, nonatomic) PMTPhotoStorage *photoStorageFilteredByCategories;
@property(strong, nonatomic) PMTPhotoStorage *photoStorageFilteredByCategoriesAndSearch;

@property(assign, nonatomic) bool isUpdating;

- (void)filterWithActiveCategories;
- (void)filterWithSearchString:(NSString *)searchString;
- (void)receivePhotosWithBlock:(ParseDataReceivedBlock)onParseDataReceived;

@end