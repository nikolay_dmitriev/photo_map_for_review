//
// Created by Nikolay Dmitriev on 12/14/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Parse/PFQuery.h>
#import "PMTDataSourceTimelineParse.h"
#import "PMTPhotoStorage.h"
#import "PMTCoreDataPhoto.h"
#import "PMTViewUtilities.h"
#import "PMTCoreDataStorage.h"
#import "PMTCoreDataUser.h"

@interface PMTDataSourceTimelineParse ()

@end

@implementation PMTDataSourceTimelineParse

#pragma mark - Custom Accessors

- (PMTPhotoStorage *)photoStorageFilteredByCategories {
    if (!_photoStorageFilteredByCategories) {
        _photoStorageFilteredByCategories = [self.photoStorage photoStorageFilteredWithActiveCategories];
    }
    return _photoStorageFilteredByCategories;
}

#pragma mark - Public

- (void)filterWithActiveCategories {
    self.photoStorageFilteredByCategories = [self.photoStorage photoStorageFilteredWithActiveCategories];
}

- (void)filterWithSearchString:(NSString *)searchString {
    self.photoStorageFilteredByCategoriesAndSearch = [self.photoStorageFilteredByCategories photoStorageFilteredWithSearchString:searchString];
}

- (void)receivePhotosWithBlock:(ParseDataReceivedBlock)onParseDataReceived{
    if(self.isUpdating){
        return;
    }
    //ToDo : Implement smart refreshing here. Consinder remove it completely
    self.isUpdating = YES;
//
    PMTCoreDataUser *user = [[PMTCoreDataStorage sharedInstance] currentUser];
    NSArray *receivedPhotos = [user.photos allObjects];
    self.photoStorage = [[PMTPhotoStorage alloc] initWithPhotoArray:receivedPhotos];
    onParseDataReceived();

    self.isUpdating = NO;

//    PFQuery *allUserPhotosQuery = [PFQuery queryWithClassName:[PMTCoreDataPhoto parseClassName]];
//    [allUserPhotosQuery whereKey:@"user" equalTo:[PFUser currentUser]];
//    [allUserPhotosQuery addDescendingOrder:@"date"];
//    allUserPhotosQuery.limit = 20;
//
//    if([self.photoStorage.allPhotosInStorage count]){
//        PMTCoreDataPhoto *lastPhoto = [self.photoStorage.allPhotosInStorage lastObject];
//        [allUserPhotosQuery whereKey:@"date" lessThan:lastPhoto.date];
//    }
//
//    __weak PMTDataSourceTimelineParse *weakSelf = self;
//    [allUserPhotosQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            _photoStorageFilteredByCategories = nil;
//
//            NSArray *receivedPhotos = objects;
//            NSArray *currentPhotos = weakSelf.photoStorage.allPhotosInStorage;
//
//            NSArray *previousAndNewPhotos = [receivedPhotos arrayByAddingObjectsFromArray:currentPhotos];
//
//            weakSelf.photoStorage = [[PMTPhotoStorage alloc] initWithPhotoArray:previousAndNewPhotos];
//            weakSelf.isUpdating = NO;
//            onParseDataReceived();
//        } else {
//            [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Error"
//                                                             withText:error.description];
//        }
//    }];
}
@end