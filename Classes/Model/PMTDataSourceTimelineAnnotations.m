//
// Created by Nikolay Dmitriev on 12/14/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTDataSourceTimelineAnnotations.h"
#import "PMTPhotoStorage.h"


@implementation PMTDataSourceTimelineAnnotations

#pragma mark - Parent Overload

- (void)receivePhotosWithBlock:(ParseDataReceivedBlock)onParseDataReceived {
    //we have offline photos
}

@end