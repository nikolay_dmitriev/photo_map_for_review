//
// Created by Nikolay Dmitriev on 12/22/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTImageCache.h"
#import "PMTConstants.h"

@interface PMTImageCache ()

@property(strong, nonatomic) NSMutableArray *keys;
@property(strong, nonatomic) NSMutableDictionary *imageCache;

@end

@implementation PMTImageCache

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Custom Accessors

- (NSMutableArray *)keys {
    if (!_keys) {
        _keys = [NSMutableArray new];
    }
    return _keys;
}

- (NSMutableDictionary *)imageCache {
    if (!_imageCache) {
        _imageCache = [NSMutableDictionary new];
    }
    return _imageCache;
}

#pragma mark - Public

- (void)addImageWithKey:(NSString *)key image:(UIImage *)image {
    if (!self.imageCache[key]) {
        [self addNewImageWithKey:key image:image];
        [self tryToDeleteOldData];
    }
}

- (UIImage *)tryGetImageFromCacheWithKey:(NSString *)key {
    return self.imageCache[key];
}

#pragma mark - Private

- (void)tryToDeleteOldData{
    if ([self.keys count] > PMTCacheSize) {
        NSString *oldKey = self.keys[0];
        [self.keys removeObjectAtIndex:0];
        self.imageCache[oldKey] = nil;
    }
}

- (void)addNewImageWithKey:(NSString *)key image:(UIImage *)image {
    self.imageCache[key] = image;
    [self.keys addObject:key];
}

@end