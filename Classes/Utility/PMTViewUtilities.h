//
//  PMTViewUtilities.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/21/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PMTViewUtilities : NSObject

@property(nonatomic, weak) UIView *view;

+ (instancetype)sharedInstance;

- (void)showLoadingView:(UIView *)sender;

- (void)dismissLoadingView;

- (void)showAlertViewWithTitle:(NSString *)title withText:(NSString *)message;

@end
