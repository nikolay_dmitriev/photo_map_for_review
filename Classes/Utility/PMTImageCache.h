//
// Created by Nikolay Dmitriev on 12/22/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMTImageCache : NSObject

+ (instancetype)sharedInstance;

- (void)addImageWithKey:(NSString *)key image:(UIImage *)image;

- (UIImage *)tryGetImageFromCacheWithKey:(NSString *)key;

@end