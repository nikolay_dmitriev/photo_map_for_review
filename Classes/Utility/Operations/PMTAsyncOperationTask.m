//
// Created by mac-184 on 1/18/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTAsyncOperationTask.h"
#import "PMTFriendsInviteParse.h"
#import "PMTPhotoParse.h"
#import "PMTCoreDataStorage.h"

@interface PMTAsyncOperationTask ()
// 'executing' and 'finished' exist in NSOperation, but are readonly
@property(atomic, assign) BOOL _executing;
@property(atomic, assign) BOOL _finished;

@property (strong, nonatomic) PMTCoreDataPhoto *photo;

@end

@implementation PMTAsyncOperationTask

- (instancetype)initWithCoreData:(PMTCoreDataPhoto *)photo {
    if(self = [super init]){
        self.photo = photo;
    }
    return self;
}

- (void)start; {
    if ([self isCancelled]) {
        // Move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        self._finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }

    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    self._executing = YES;
    [self didChangeValueForKey:@"isExecuting"];

    PMTPhotoParse *photoParse = [PMTPhotoParse parsePhotoWithCoreDataPhoto:self.photo];

    NSLog(@"Starting to upload photo");
    __weak PMTAsyncOperationTask *weakSelf = self;
    [photoParse saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

        NSLog(@"Completed ");
        weakSelf.photo.parseId = photoParse.objectId;
        [[PMTCoreDataStorage sharedInstance] saveContext];

        [weakSelf completeOperation];
    }];
}

- (void)main; {
    if ([self isCancelled]) {
        return;
    }
}

- (BOOL)isAsynchronous; {
    return YES;
}

- (BOOL)isExecuting {
    return self._executing;
}

- (BOOL)isFinished {
    return self._finished;
}

- (void)completeOperation {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];

    self._executing = NO;
    self._finished = YES;

    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

@end