//
// Created by mac-184 on 1/13/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMTTestOperation;
@class PMTCoreDataPhoto;

@interface PMTOperationQueue : NSObject

+ (instancetype)sharedInstance;

- (void)addTask:(NSOperation *)task;

- (void)uploadInvitesForCoreDataPhoto:(PMTCoreDataPhoto *)photo invites:(NSArray *)invites;

@property(assign, nonatomic, readonly, getter=isEmpty) BOOL empty;

@end