//
// Created by mac-184 on 1/13/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTUploadInviteFriendsToParseTask.h"
#import "PMTCoreDataPhoto.h"
#import "PMTFriendsInviteParse.h"

@interface PMTUploadInviteFriendsToParseTask ()

@property(strong, nonatomic) PMTCoreDataPhoto *photo;
@property(strong, nonatomic) NSArray *contacts;

@end

@implementation PMTUploadInviteFriendsToParseTask

- (instancetype)initWithCoreData:(PMTCoreDataPhoto *)photo contacts:(NSArray *)selectedContacts {
    if (self = [super init]) {
        self.photo = photo;
        self.contacts = selectedContacts;
    }
    return self;
}

- (void)main {

    if (self.cancelled) return;

    PMTFriendsInviteParse *invite = [PMTFriendsInviteParse object];

    invite.inviteForPhoto = self.photo.parseId;
    invite.invitedUsers = self.contacts;
    invite.inviteOwner = [PFUser currentUser];

    if (self.cancelled) return;

    NSLog(@"Starting uploading invite for users : ");
    [self.contacts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSLog(@"%@", obj);
    }];

    __weak PMTUploadInviteFriendsToParseTask *weakSelf = self;
    [invite saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        NSLog(@"Completed uploading invite for users : ");
        [invite.invitedUsers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSLog(@"%@", obj);
        }];
    }];
}

@end