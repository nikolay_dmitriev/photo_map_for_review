//
// Created by mac-184 on 1/13/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTOperationQueue.h"
#import "PMTCoreDataPhoto.h"
#import "PMTUploadPhotoToParseTask.h"
#import "PMTUploadInviteFriendsToParseTask.h"
#import "PMTAsyncOperationTask.h"

@interface PMTOperationQueue ()

@property(strong, nonatomic) NSOperationQueue *operationQueue;

@end

@implementation PMTOperationQueue

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Custom Accessors

- (NSOperationQueue *)operationQueue {
    if (!_operationQueue) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.name = @"PMTParseQueue";
//        _operationQueue.maxConcurrentOperationCount = 1;
    }
    return _operationQueue;
}

#pragma mark - Public

- (void)addTask:(NSOperation *)task {
    [self.operationQueue addOperation:task];
}

- (void)uploadInvitesForCoreDataPhoto:(PMTCoreDataPhoto *)photo invites:(NSArray *)invites {
    //if we have no invites
    if (![invites count]) {
        return;
    }

    if (photo.parseId) {
        [self sendInvitesToParseForPhoto:photo invites:invites];
    } else {
        [self uploadAndSendInvitesToParse:photo invites:invites];
    }
}

- (BOOL)isEmpty {
    return self.operationQueue.operationCount == 0;
}

#pragma mark - Private

- (void)uploadAndSendInvitesToParse:(PMTCoreDataPhoto *)photo invites:(NSArray *)invites {
    PMTAsyncOperationTask *uploadPhotoToParse = [[PMTAsyncOperationTask alloc] initWithCoreData:photo];

    [invites enumerateObjectsUsingBlock:^(NSArray *contactsList, NSUInteger idx, BOOL *stop) {
        if ([contactsList count]) {
            PMTUploadInviteFriendsToParseTask *uploadInviteToParse = [[PMTUploadInviteFriendsToParseTask alloc] initWithCoreData:photo contacts:contactsList];
            [uploadInviteToParse addDependency:uploadPhotoToParse];
            [self.operationQueue addOperation:uploadInviteToParse];
        }
    }];

    [self.operationQueue addOperation:uploadPhotoToParse];
}

- (void)sendInvitesToParseForPhoto:(PMTCoreDataPhoto *)photo invites:(NSArray *)invites {
    [invites enumerateObjectsUsingBlock:^(NSArray *contactsList, NSUInteger idx, BOOL *stop) {
        if ([contactsList count]) {
            PMTUploadInviteFriendsToParseTask *uploadInviteToParse = [[PMTUploadInviteFriendsToParseTask alloc] initWithCoreData:photo contacts:contactsList];
            [self.operationQueue addOperation:uploadInviteToParse];
        }
    }];
}

@end