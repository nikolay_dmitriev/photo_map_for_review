//
// Created by mac-184 on 1/13/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMTCoreDataPhoto;

@interface PMTDownloadPhotoFromParseTask : NSOperation

- (instancetype)initWithCoreData:(PMTCoreDataPhoto *)photo;

@end