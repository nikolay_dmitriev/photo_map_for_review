//
// Created by mac-184 on 1/13/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTDownloadPhotoFromParseTask.h"
#import "PMTCoreDataPhoto.h"
#import "PMTPhotoParse.h"

@interface PMTDownloadPhotoFromParseTask()

@property(strong, nonatomic) PMTCoreDataPhoto *photo;
@property(strong, nonatomic) PMTPhotoParse *downloadedPhoto;

@end

@implementation PMTDownloadPhotoFromParseTask

- (instancetype)initWithCoreData:(PMTCoreDataPhoto *)photo{
    if (self = [super init]) {
        self.photo = photo;
    }
    return self;
}

- (void)main {
    if (self.cancelled) return;

    PFQuery *query = [PFQuery queryWithClassName:[PMTPhotoParse parseClassName]];

    BFTask *task = [query getObjectInBackgroundWithId:self.photo.parseId];

    [task waitUntilFinished];

    self.downloadedPhoto = task.result;

    NSLog(@"Download photo with description %@", self.downloadedPhoto.photoDescription);

    if (self.cancelled) return;
}

@end