//
// Created by mac-184 on 1/18/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMTCoreDataPhoto.h"

@interface PMTAsyncOperationTask : NSOperation

- (instancetype)initWithCoreData:(PMTCoreDataPhoto *)photo;

@end