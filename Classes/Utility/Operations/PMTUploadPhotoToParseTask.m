//
// Created by mac-184 on 1/13/16.
// Copyright (c) 2016 Nikolay Dmitriev. All rights reserved.
//

#import "PMTUploadPhotoToParseTask.h"
#import "PMTPhotoParse.h"
#import "PMTCoreDataPhoto.h"
#import "PMTCoreDataStorage.h"

@interface PMTUploadPhotoToParseTask()

@property (strong, nonatomic) PMTCoreDataPhoto *photo;

@end

@implementation PMTUploadPhotoToParseTask

- (instancetype)initWithCoreData:(PMTCoreDataPhoto *)photo {
    if(self = [super init]){
        self.photo = photo;
    }
    return self;
}

- (void)main {
    PMTPhotoParse *parsePhoto = [PMTPhotoParse parsePhotoWithCoreDataPhoto:self.photo];

    if (self.cancelled) return;

    //If the upload task for this invites is already finished
    //For now it's only 1 task
    if (self.photo.parseId) [self cancel];

    BFTask *uploadingTask = [parsePhoto saveInBackground];

    if (self.cancelled) return;

    [uploadingTask waitUntilFinished];

    self.photo.parseId = parsePhoto.objectId;

    [[PMTCoreDataStorage sharedInstance] saveContext];
}

@end