//
// Created by Nikolay Dmitriev on 12/3/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoMapAnnotation.h"
#import "PMTFullPhotoPreviewFooterView.h"
#import "PMTConstants.h"
#import "PMTPhotoMapAnnotationCalloutView.h"
#import "PMTCoreDataPhoto.h"

@interface PMTPhotoMapAnnotation()

@property (weak, nonatomic) UIView *calloutView;

@end

@implementation PMTPhotoMapAnnotation

#pragma mark - Custom Accessors

- (void)setPhoto:(PMTCoreDataPhoto *)photo {
    _photo = photo;
    //ToDo : Make categories here
    CLLocationCoordinate2D annotationLocation = CLLocationCoordinate2DMake(photo.locationLatitude.doubleValue, photo.locationLongitude.doubleValue);
    self.location = annotationLocation;
}

#pragma mark - Public

- (CLLocationCoordinate2D)coordinate {
    return self.location;
}

- (void)showCallout:(UIView *)view {
    if(self.calloutView){
        return;
    }

    PMTPhotoMapAnnotationCalloutView *callout = [[[NSBundle mainBundle] loadNibNamed:PMTPhotoMapAnnotationCalloutXibName owner:self options:nil] firstObject];

    callout.photoAnnotationCalloutTapped = self.onPhotoAnnotationCalloutTapped;

    callout.frame = CGRectMake(0, 0, 250, 70);
    [view addSubview:callout];
    callout.center = CGPointMake(view.bounds.size.width * 0.5f, -view.bounds.size.height * 1.1f);

    [callout setupWithPhoto:self.photo];

    self.calloutView = callout;
}

- (void)hideCallout {
    [self.calloutView removeFromSuperview];
}

@end