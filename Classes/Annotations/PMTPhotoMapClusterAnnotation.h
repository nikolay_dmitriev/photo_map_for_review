//
// Created by Nikolay Dmitriev on 12/15/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface PMTPhotoMapClusterAnnotation : MKAnnotationView

@property (weak, nonatomic) UILabel *numberOfPhotosLabel;

@end