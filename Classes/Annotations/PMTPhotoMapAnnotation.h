//
// Created by Nikolay Dmitriev on 12/3/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "PMTPhotoMapAnnotationCalloutView.h"

@class PMTCoreDataPhoto;

@interface PMTPhotoMapAnnotation : NSObject <MKAnnotation>

@property(strong, nonatomic) PMTCoreDataPhoto *photo;

@property(assign, nonatomic) CLLocationCoordinate2D location;

@property(copy, nonatomic) PhotoAnnotationCalloutTappedForPhoto onPhotoAnnotationCalloutTapped;

- (void)showCallout:(UIView *)view;

- (void)hideCallout;

@end