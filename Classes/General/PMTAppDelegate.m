//
//  PMTAppDelegate.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/17/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTAppDelegate.h"
#import "PMTSignInViewController.h"
#import "Parse.h"
#import "PMTConstants.h"
#import "PMTInviteFriendsViewController.h"
#import "PMTUserContactsStorage.h"
#import "PMTUserContact.h"
#import "PMTInviteFriendsViewModel.h"
#import "PMTOperationQueue.h"

@interface PMTAppDelegate ()

@end

@implementation PMTAppDelegate

#pragma mark - ApplicationCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self initParse];
    [self showStartScreen];


    //[[PMTCoreDataStorage sharedInstance] drop];
//    [[PMTCoreDataStorage sharedInstance] show];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Public

- (void)showApplicationView {
    self.window.rootViewController = [[UIStoryboard storyboardWithName:PMTMainStoryboardName
                                                                bundle:[NSBundle mainBundle]]
            instantiateInitialViewController];
}

- (void)showLoginView {
    UIViewController *rootController = [[PMTSignInViewController alloc] initWithNibName:PMTSingInXibName bundle:[NSBundle mainBundle]];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:rootController];
    self.window.rootViewController = navigationController;
}

#pragma mark - Private

- (void)initParse {
    [Parse setApplicationId:PMTParseApplicationId clientKey:PMTParseClientKey];
}

- (void)showStartScreen {
    BOOL isUserAuthenticated = [PFUser currentUser] != nil;

    if (isUserAuthenticated) {
        [self showApplicationView];
    } else {
        [self showLoginView];
    }
}

@end
