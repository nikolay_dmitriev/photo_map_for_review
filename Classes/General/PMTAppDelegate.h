//
//  PMTAppDelegate.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/17/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMTAppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

- (void)showApplicationView;

- (void)showLoginView;

@end