//
//  PMTConstants.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/21/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTConstants.h"

#pragma mark - Xib IDs

NSString *const PMTSingInXibName = @"SignIn";
NSString *const PMTSignUpXibName = @"SignUp";
NSString *const PMTCategoriesXibName = @"Categories";
NSString *const PMTFullPhotoPreviewXibName = @"FullPhotoPreview";
NSString *const PMTPhotoMapPreviewFooterXibName = @"FullPhotoPreviewFooter";
NSString *const PMTPhotoMapAnnotationCalloutXibName = @"PhotoMapAnnotationCallout";
NSString *const PMTPhotoMapPopupXibName = @"PhotoMapPopup";
NSString *const PMTInviteFriendsXibName = @"InviteFriends";

#pragma mark - Storyboard IDs

NSString *const PMTMainStoryboardName = @"Main";

#pragma mark - Parse keys

NSString *const PMTParseApplicationId = @"rP44eIxrNzUXhCZ5KeLM2atVHrFshW8CQd10fU7F";
NSString *const PMTParseClientKey = @"XUoQbDltHy2enEHV8sw3wSBTuy2fTEPUja1gPzKq";

#pragma mark - TableView Cells

NSString *const PMTPPhotoCategoryTableViewCellXibName = @"CategoryTableViewCell";
NSString *const PMTTimelineTableViewCellXibName = @"TimelineTableViewCell";
NSString *const PMTTimelineTableViewCellExtendedXibName = @"TimelineTableViewCellExtended";

#pragma mark - Segues

NSString *const PMTFromAnnotationToTimelineSegue = @"ShowTimelineFromClusterAnnotationSegue";

#pragma mark - Constants

NSInteger const PMTCacheSize = 4;