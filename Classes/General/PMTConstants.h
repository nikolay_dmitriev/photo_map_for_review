//
//  PMTConstants.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/21/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Xib IDs

extern NSString *const PMTSingInXibName;
extern NSString *const PMTSignUpXibName;
extern NSString *const PMTCategoriesXibName;
extern NSString *const PMTFullPhotoPreviewXibName;
extern NSString *const PMTPhotoMapPreviewFooterXibName;
extern NSString *const PMTPhotoMapAnnotationCalloutXibName;
extern NSString *const PMTPhotoMapPopupXibName;
extern NSString *const PMTInviteFriendsXibName;

#pragma mark - Storyboard IDs

extern NSString *const PMTMainStoryboardName;

#pragma mark - Parse keys

extern NSString *const PMTParseApplicationId;
extern NSString *const PMTParseClientKey;

#pragma mark - TableView Cells

extern NSString *const PMTPPhotoCategoryTableViewCellXibName;
extern NSString *const PMTTimelineTableViewCellXibName;
extern NSString *const PMTTimelineTableViewCellExtendedXibName;

#pragma mark - Segues

extern NSString *const PMTFromAnnotationToTimelineSegue;

#pragma mark - Constants

extern NSInteger const PMTCacheSize;