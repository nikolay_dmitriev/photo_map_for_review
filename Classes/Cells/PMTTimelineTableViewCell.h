//
//  PMTTimelineTableViewCell.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/30/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMTCoreDataPhoto;
@class PFImageView;
@class PMTPhotoImageView;

@interface PMTTimelineTableViewCell : UITableViewCell

@property(weak, nonatomic) IBOutlet PMTPhotoImageView *photoImageView;
@property(weak, nonatomic) IBOutlet UILabel *photoDateAndCategoryLabel;

- (void)updateWithPhoto:(PMTCoreDataPhoto *)photo;

@end
