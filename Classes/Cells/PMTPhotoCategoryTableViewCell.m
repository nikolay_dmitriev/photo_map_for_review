//
//  PMTPhotoCategoryTableViewCell.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/27/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoCategoryTableViewCell.h"
#import "PMTPhotoCategory.h"

@implementation PMTPhotoCategoryTableViewCell

#pragma mark - Lifecycle

- (void)awakeFromNib {
    [self makeCategoryStatusButtonRound];
    [self addStatusButtonActionToSelf];
    [super awakeFromNib];
}

#pragma mark - Public

- (void)setCellStyleForCategory:(PMTPhotoCategory *)category {
    [self setLabelText:category.title color:category.color];

    if (category.isActive) {
        [self drawFilledButtonWithColor:category.color];
    } else {
        [self drawOnlyButtonBorderWithColor:category.color];
    }
}

#pragma mark - Actions

- (void)onStatusButtonTapped {
    self.onStatusButtonTap(self);
}

#pragma mark - Private

- (void)makeCategoryStatusButtonRound {
    self.categoryStatusButton.layer.cornerRadius = self.categoryStatusButton.frame.size.width / 2;
    self.categoryStatusButton.layer.borderWidth = 2;
}

- (void)addStatusButtonActionToSelf {
    [self.categoryStatusButton addTarget:self action:@selector(onStatusButtonTapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setLabelText:(NSString *)text color:(UIColor *)color {
    self.categoryNameLabel.text = text;
    [self.categoryNameLabel setTextColor:color];
}

- (void)drawFilledButtonWithColor:(UIColor *)color {
    [self.categoryStatusButton setBackgroundColor:color];
    [self.categoryStatusButton.layer setBorderColor:[color CGColor]];
}

- (void)drawOnlyButtonBorderWithColor:(UIColor *)color {
    [self.categoryStatusButton setBackgroundColor:[UIColor clearColor]];
    [self.categoryStatusButton.layer setBorderColor:[color CGColor]];
}

@end
