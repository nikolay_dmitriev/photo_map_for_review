//
//  PMTTimelineTableViewCellExtended.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 12/1/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMTTimelineTableViewCell.h"

@interface PMTTimelineTableViewCellExtended : PMTTimelineTableViewCell

@property(weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
