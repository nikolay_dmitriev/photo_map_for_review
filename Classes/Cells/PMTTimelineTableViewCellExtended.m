//
//  PMTTimelineTableViewCellExtended.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 12/1/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTTimelineTableViewCellExtended.h"
#import "PMTCoreDataPhoto.h"

@implementation PMTTimelineTableViewCellExtended

- (void)updateWithPhoto:(PMTCoreDataPhoto *)photo {
    self.descriptionLabel.text = photo.photoDescription;
    [super updateWithPhoto:photo];
}

@end
