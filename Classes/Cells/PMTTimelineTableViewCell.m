//
//  PMTTimelineTableViewCell.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/30/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTTimelineTableViewCell.h"
#import "PMTPhotoCategory.h"
#import "PMTCoreDataPhoto.h"
#import "PFImageView.h"
#import "PMTPhotoImageView.h"
#import "PMTCoreDataPhotoData.h"
#import "PMTPhotoCategories.h"

@implementation PMTTimelineTableViewCell

#pragma mark - Public

- (void)updateWithPhoto:(PMTCoreDataPhoto *)photo {
    self.photoImageView.image = [UIImage imageWithData:photo.data.photoData];

    NSString *dateString = [self stringFromDate:photo.date];
    NSString *categoryString = [self stringFromCategory:(PhotoCategoriesTypes) [photo.category integerValue]];

    self.photoDateAndCategoryLabel.text = [NSString stringWithFormat:@"%@ / %@", dateString, categoryString];
}

#pragma mark - Private

- (NSString *)stringFromDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setDateFormat:@"MM-dd-yy"];

    return [dateFormatter stringFromDate:date];
}

- (NSString *)stringFromCategory:(PhotoCategoriesTypes)categoryType {
    PMTPhotoCategory *category = [[PMTPhotoCategories sharedInstance] categoryForType:categoryType];
    return category.title;
}

@end
