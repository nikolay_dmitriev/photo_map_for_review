//
//  PMTPhotoCategoryTableViewCell.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/27/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMTPhotoCategories.h"


@class PMTPhotoCategoryTableViewCell;

typedef void (^CategoryTableViewButtonTappedBlock)(PMTPhotoCategoryTableViewCell *cell);

@interface PMTPhotoCategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *categoryStatusButton;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@property (copy, nonatomic) CategoryTableViewButtonTappedBlock onStatusButtonTap;

- (void)setCellStyleForCategory:(PMTPhotoCategory *)category;

@end
