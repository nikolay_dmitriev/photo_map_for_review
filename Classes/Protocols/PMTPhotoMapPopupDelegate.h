//
// Created by Nikolay Dmitriev on 11/25/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@class PMTCoreDataPhoto;
@class PMTPhotoMapPopupViewController;

@protocol PMTPhotoMapPopupDelegate

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup tapPhotoPreviewForPhoto:(PMTCoreDataPhoto *)photo withImage:(UIImage *)image;

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup tapInviteForPhoto:(PMTCoreDataPhoto *)photo withImage:(UIImage *)image;

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup didTapCancelButtonForPhoto:(PMTCoreDataPhoto *)photo;

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup didTapDoneButtonForPhoto:(PMTCoreDataPhoto *)photo;

@end