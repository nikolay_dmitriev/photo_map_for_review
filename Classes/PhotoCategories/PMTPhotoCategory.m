//
//  PMTPhotoCategory.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoCategory.h"

@implementation PMTPhotoCategory

- (instancetype)initWithTitle:(NSString *)title color:(UIColor *)color mapImage:(NSString *)mapImage {
    if (self == [super init]) {
        _title = title;
        _color = color;
        _mapImage = mapImage;
        self.active = YES;
    }
    return self;
}

@end
