//
//  PMTPhotoCategories.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@class PMTPhotoCategory;

//ToDO : Consider using enum bit flags
typedef NS_ENUM(NSInteger, PhotoCategoriesTypes) {

    PhotoCategoryTypeDefault = 0,

    PhotoCategoryTypeFriends = 1,

    PhotoCategoryTypeNature = 2,

};

@interface PMTPhotoCategories : NSObject

+ (instancetype)sharedInstance;

- (PMTPhotoCategory *)categoryForType:(PhotoCategoriesTypes)type;

- (void)resetCategoriesActiveStatus;

@property(readonly, nonatomic) NSInteger count;

@end
