//
//  PMTPhotoCategory.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface PMTPhotoCategory : NSObject

@property(readonly, nonatomic) NSString *title;
@property(readonly, nonatomic) NSString *mapImage;
@property(readonly, nonatomic) UIColor *color;
@property(assign, nonatomic, getter = isActive) BOOL active;

- (instancetype)initWithTitle:(NSString *)title color:(UIColor *)color mapImage:(NSString *)mapImage;

@end
