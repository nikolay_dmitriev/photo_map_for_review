//
//  PMTPhotoCategories.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoCategories.h"
#import "PMTPhotoCategory.h"
#import "PMTUIColorCategory.h"

@interface PMTPhotoCategories ()

@property(strong, nonatomic) NSDictionary *categories;

@end

@implementation PMTPhotoCategories

#pragma mark - Singleton

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Custom Accessors

- (NSDictionary *)categories {
    if (!_categories)
        _categories = @{@0 : [[PMTPhotoCategory alloc] initWithTitle:@"DEFAULTS"
                                                               color:[UIColor colorWithHex:@"#368EDF"]
                                                            mapImage:@"MarkerDefault"],
                @1 : [[PMTPhotoCategory alloc] initWithTitle:@"FRIENDS"
                                                       color:[UIColor colorWithHex:@"#F4A523"]
                                                    mapImage:@"MarkerFriends"],
                @2 : [[PMTPhotoCategory alloc] initWithTitle:@"NATURE"
                                                       color:[UIColor colorWithHex:@"#578E18"]
                                                    mapImage:@"MarkerNature"]};
    return _categories;
}

- (NSInteger)count {
    return [self.categories count];
}

#pragma mark - Public

- (PMTPhotoCategory *)categoryForType:(PhotoCategoriesTypes)type {
    NSNumber *key = @(type);
    return self.categories[key];
}

- (void)resetCategoriesActiveStatus {
    [self.categories enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        PMTPhotoCategory *category = obj;
        category.active = YES;
    }];
}

@end
