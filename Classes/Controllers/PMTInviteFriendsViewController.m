//
//  PMTInviteFriendsViewController.m
//  PhotoMap
//
//  Created by mac-184 on 1/12/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//

#import <ContactsUI/ContactsUI.h>
#import "PMTInviteFriendsViewController.h"
#import "PMTUserContact.h"
#import "PMTUserContactsStorage.h"
#import "PMTInviteFriendsViewModel.h"

@interface PMTInviteFriendsViewController ()

@property(strong, nonatomic) PMTInviteFriendsViewModel *inviteFriendsViewModel;

@property(weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PMTInviteFriendsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"InviteCell"];
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self.inviteFriendsViewModel fetchContactsInBackgroundWithBlock:^{
        __weak PMTInviteFriendsViewController *weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView reloadData];
        });
    }];
}

#pragma mark - Custom Accessors

- (PMTInviteFriendsViewModel *)inviteFriendsViewModel {
    if (!_inviteFriendsViewModel) _inviteFriendsViewModel = [PMTInviteFriendsViewModel new];
    return _inviteFriendsViewModel;
}

#pragma mark - Actions

- (IBAction)doneTapped:(id)sender {
    self.onInviteDoneTapped(self.inviteFriendsViewModel.invitedContacts);
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.inviteFriendsViewModel numberOfSectionsInTable];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.inviteFriendsViewModel sectionHeaderForIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.inviteFriendsViewModel numberOfRowsInSectionWithIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InviteCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    PMTUserContact *contact = [self.inviteFriendsViewModel contactForIndex:indexPath];

    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", contact.firstname, contact.lastname];
    cell.backgroundColor = contact.isInvited ? [UIColor greenColor] : [UIColor whiteColor];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PMTUserContact *contact = [self.inviteFriendsViewModel contactForIndex:indexPath];

    contact.isInvited = !contact.isInvited;

    [self.tableView reloadData];
}

#pragma mark - Private

@end