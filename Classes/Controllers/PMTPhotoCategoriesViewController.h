//
//  PMTPhotoCategoriesViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CategoriesViewControllerDismissBlock)();

@interface PMTPhotoCategoriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(copy, nonatomic) CategoriesViewControllerDismissBlock onViewControllerDismiss;

@end