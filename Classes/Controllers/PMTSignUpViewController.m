//
//  PMTSignUpViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/19/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTSignUpViewController.h"
#import "Parse/Parse.h"
#import "PMTViewUtilities.h"
#import "PMTAppDelegate.h"

@interface PMTSignUpViewController ()

@property(weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property(weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property(weak, nonatomic) IBOutlet UITextField *passwordConfirmationTextField;

@end

@implementation PMTSignUpViewController

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self registerForKeyboardEvents];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterForKeyboardNotifications];
}

#pragma mark - Actions

- (IBAction)singUpButtonTapped:(id)sender {
    bool isPasswordMatch = [self.passwordTextField.text isEqualToString:self.passwordConfirmationTextField.text];
    if (!isPasswordMatch) {
        [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Error"
                                                         withText:@"Password doesn't match"];
        return;
    }

    [self sendSignUpRequest];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self resetPosition];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self resetPosition];
    return YES;
}

#pragma mark - Keyboard Notifications

- (void)keyboardWasShown:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];

    CGRect keyboardRect = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect visibleRect = self.passwordConfirmationTextField.frame;

    float visibleViewBottom = visibleRect.origin.y + visibleRect.size.height;
    float animationHeight = keyboardRect.origin.y - visibleViewBottom;

    bool isKeyboardUnderTextField = animationHeight >= 0;
    if (isKeyboardUnderTextField) {
        return;
    }

    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.view setTransform:CGAffineTransformMakeTranslation(0, animationHeight)];
    }                completion:nil];
}


#pragma mark - Private

- (void)sendSignUpRequest {
    PFUser *user = [PFUser user];

    user.username = self.usernameTextField.text;
    user.password = self.passwordTextField.text;

    [[PMTViewUtilities sharedInstance] showLoadingView:self.view];

    __weak PMTSignUpViewController *weakSelf = self;
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [[PMTViewUtilities sharedInstance] dismissLoadingView];
        if (!error) {
            [weakSelf onSuccessfulRegistration];
        } else {
            [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Error"
                                                             withText:error.description];
        }
    }];
}

- (void)onSuccessfulRegistration {
    PMTAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate showApplicationView];
}

- (void)registerForKeyboardEvents {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)deregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
}

- (void)resetPosition {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    }                completion:nil];
}


@end
