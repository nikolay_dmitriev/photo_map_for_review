//
//  PMTPhotoMapPopupViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/24/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMTPhotoMapPopupDelegate;
@class PMTCoreDataPhoto;

@interface PMTPhotoMapPopupViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property(weak, nonatomic) id <PMTPhotoMapPopupDelegate> popupDelegate;

- (void)initWithPhoto:(PMTCoreDataPhoto *)photo image:(UIImage *)image;

@end
