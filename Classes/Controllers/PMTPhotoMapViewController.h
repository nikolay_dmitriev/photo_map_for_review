//
//  PMTPhotoMapViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PMTPhotoMapPopupDelegate.h"

@interface PMTPhotoMapViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, PMTPhotoMapPopupDelegate, CLLocationManagerDelegate, MKMapViewDelegate>

@end
