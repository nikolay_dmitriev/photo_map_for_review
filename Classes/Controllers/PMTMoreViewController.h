//
//  PMTMoreViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/21/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMTMoreViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(weak, nonatomic) IBOutlet UITableView *tableView;

@end
