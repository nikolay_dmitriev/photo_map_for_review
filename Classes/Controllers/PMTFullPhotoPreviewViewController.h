//
//  PMTFullPhotoPreviewViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/23/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMTCoreDataPhoto;

typedef void (^FullPhotoPreviewDismissBlock)();

@interface PMTFullPhotoPreviewViewController : UIViewController <UIScrollViewDelegate>

- (void)setupWithPhoto:(PMTCoreDataPhoto *)photo image:(UIImage *)image;

@property(copy, nonatomic) FullPhotoPreviewDismissBlock onViewControllerDismiss;

@end
