//
//  PMTPhotoMapPopupViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/24/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <ParseUI/PFImageView.h>
#import "PMTPhotoMapPopupViewController.h"
#import "PMTPhotoMapPopupDelegate.h"
#import "PMTPhotoCategories.h"
#import "PMTPhotoCategory.h"
#import "PMTCoreDataPhoto.h"
#import "PMTPhotoImageView.h"
#import "PMTCoreDataPhotoData.h"

@interface PMTPhotoMapPopupViewController ()

@property(weak, nonatomic) IBOutlet PMTPhotoImageView *photoPreviewImageView;
@property(weak, nonatomic) IBOutlet UIView *darkBackgroundView;

@property(weak, nonatomic) IBOutlet UIView *internalPopupView;

@property(weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property(weak, nonatomic) IBOutlet UIPickerView *categoriesPickerView;

@property(weak, nonatomic) IBOutlet UIImageView *categoryOnMapImageView;
@property(weak, nonatomic) IBOutlet UILabel *categoryNameLabel;

@property(weak, nonatomic) IBOutlet UILabel *photoDateLabel;

@property(strong, nonatomic) PMTCoreDataPhoto *photoPreview;

@property(strong, nonatomic) UIImage *imageForPhoto;
@end

@implementation PMTPhotoMapPopupViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateUi];

    [self registerForKeyboardEvents];

    [self createKeyboardToolbar];

    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.75 animations:^{
        self.darkBackgroundView.alpha = 0.7;
    }];

    [self loadPreviewImage];
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    self.darkBackgroundView.alpha = 0;
    [self deregisterForKeyboardNotifications];
    [super viewWillDisappear:animated];
}

#pragma mark - Actions

- (IBAction)doneButtonTapped:(id)sender {
    //If done was tapped when keyboard is on screen - handle it
    if (self.descriptionTextView.isFirstResponder) {
        [self hideKeyboard];
        return;
    }

    self.photoPreview.photoDescription = self.descriptionTextView.text;
    [self.popupDelegate photoMapPopup:self didTapDoneButtonForPhoto:self.photoPreview];
}


- (IBAction)cancelButtonTapped:(id)sender {
    [self.popupDelegate photoMapPopup:self didTapCancelButtonForPhoto:self.photoPreview];
}

- (IBAction)categoriesButtonTapped:(id)sender {
    [self.categoriesPickerView setAlpha:1];
}

- (IBAction)imageViewTapped:(id)sender {
    if (self.photoPreviewImageView.image) {
        [self.popupDelegate photoMapPopup:self tapPhotoPreviewForPhoto:self.photoPreview withImage:self.photoPreviewImageView.image];
    }
}

- (IBAction)inviteButtonTapped:(id)sender {
    [self.popupDelegate photoMapPopup:self tapInviteForPhoto:self.photoPreview withImage:self.photoPreviewImageView.image];
}

- (void)keyboardDoneTapped {
    [self hideKeyboard];
}

#pragma mark - Public

- (void)initWithPhoto:(PMTCoreDataPhoto *)photo image:(UIImage *)image {
    self.imageForPhoto = image;
    self.photoPreview = photo;
}

#pragma mark - KeyboardNotifications

- (void)keyboardWasShown:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];

    CGRect keyboardRect = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect visibleRect = self.internalPopupView.frame;

    float visibleViewBottom = visibleRect.origin.y + visibleRect.size.height;
    float animationHeight = keyboardRect.origin.y - visibleViewBottom;

    bool isKeyboardUnderTextField = animationHeight >= 0;
    if (isKeyboardUnderTextField) {
        return;
    }

    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.view setTransform:CGAffineTransformMakeTranslation(0, animationHeight)];
    }                completion:nil];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[PMTPhotoCategories sharedInstance] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) row].title;
}

#pragma mark - UIPickerViewDelegate

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    PhotoCategoriesTypes photoCategoryType = (PhotoCategoriesTypes) row;

    PMTPhotoCategory *category = [[PMTPhotoCategories sharedInstance] categoryForType:photoCategoryType];

    UIImage *img = [UIImage imageNamed:category.mapImage];
    UIImageView *temp = [[UIImageView alloc] initWithImage:img];
    temp.contentMode = UIViewContentModeScaleAspectFit;

    float viewContainerHeight = 100;
    float viewContainerWidth = self.categoriesPickerView.frame.size.width;

    float imageHeight = img.size.height;
    float imageWidth = img.size.width;
    float imageOffsetX = viewContainerWidth * 0.1f;
    float imageOffsetY = viewContainerHeight / 2.0f - imageHeight / 2.0f;

    float labelWidth = 110;
    float labelHeight = 100;
    float labelPositionX = viewContainerWidth / 2.0f - labelWidth / 2.0f;
    float labelPositionY = viewContainerHeight / 2.0f - labelHeight / 2.0f;

    temp.frame = CGRectMake(imageOffsetX, imageOffsetY, imageHeight, imageWidth);

    UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelPositionX, labelPositionY, labelWidth, labelHeight)];
    channelLabel.text = category.title;
    channelLabel.backgroundColor = [UIColor clearColor];
    channelLabel.textAlignment = NSTextAlignmentCenter;

    UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, viewContainerWidth, viewContainerHeight)];
    [tmpView insertSubview:temp atIndex:0];
    [tmpView insertSubview:channelLabel atIndex:1];

    return tmpView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    PhotoCategoriesTypes photoCategory = (PhotoCategoriesTypes) row;
    self.photoPreview.category = @(photoCategory);

    //hides picker from user
    [self.categoriesPickerView setAlpha:0];
    [self updateCategoryPlaceholder];
}

#pragma mark - Private

- (void)registerForKeyboardEvents {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)deregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
}

- (void)resetPopupPosition {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    }                completion:nil];
}

- (void)hideKeyboard {
    [self.descriptionTextView resignFirstResponder];
    [self resetPopupPosition];
}

- (void)updateUi {
    self.descriptionTextView.text = self.photoPreview.photoDescription;

    [self updateDateLabel];
    [self updateCategoryPlaceholder];
}

- (void)loadPreviewImage {
    if (!self.imageForPhoto) {
        self.photoPreviewImageView.image = [UIImage imageWithData:self.photoPreview.data.photoData];
    } else {
        self.photoPreviewImageView.image = self.imageForPhoto;
    }
}

- (void)updateDateLabel {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setDateFormat:@"MMMM dd', 'YYYY '-' HH:mm a"];

    self.photoDateLabel.text = [dateFormatter stringFromDate:self.photoPreview.date];
}

- (void)updateCategoryPlaceholder {
    PMTPhotoCategory *currentCategory = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) self.photoPreview.category.intValue];
    self.categoryOnMapImageView.image = [UIImage imageNamed:currentCategory.mapImage];
    self.categoryNameLabel.text = currentCategory.title;
}

- (void)createKeyboardToolbar {
    UIToolbar *keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    keyboardToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
            [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(keyboardDoneTapped)]];
    [keyboardToolbar sizeToFit];
    keyboardToolbar.tintColor = [UIColor blackColor];
    self.descriptionTextView.inputAccessoryView = keyboardToolbar;
}


@end
