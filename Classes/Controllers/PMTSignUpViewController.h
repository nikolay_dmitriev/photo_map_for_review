//
//  PMTSignUpViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/19/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMTSignUpViewController : UIViewController <UITextFieldDelegate>

@end
