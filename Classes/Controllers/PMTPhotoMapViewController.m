//
//  PMTPhotoMapViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <OCMapView/OCMapView.h>
#import "PMTPhotoMapViewController.h"
#import "PMTPhotoCategoriesViewController.h"
#import "PMTConstants.h"
#import "PMTPhotoMapPopupViewController.h"
#import "PMTViewUtilities.h"
#import "PMTFullPhotoPreviewViewController.h"
#import "UINavigationBar+PMTUINavigationBarCategory.h"
#import "PMTPhotoMapAnnotation.h"
#import "PMTPhotoCategory.h"
#import "PMTTimelineTableViewController.h"
#import "PMTCoreDataPhoto.h"
#import "PMTDataSourceTimelineAnnotations.h"
#import "PMTPhotoStorage.h"
#import "PMTPhotoMapClusterAnnotation.h"
#import "PMTCoreDataStorage.h"
#import "PMTPhotoCategories.h"
#import "PMTCoreDataUser+CoreDataProperties.h"
#import "PMTInviteFriendsViewController.h"
#import "PFObject.h"
#import "PMTPhotoParse.h"
#import "PMTOperationQueue.h"
#import "PMTUploadPhotoToParseTask.h"
#import "PMTDownloadPhotoFromParseTask.h"
#import "PMTAsyncOperationTask.h"

@interface PMTPhotoMapViewController ()

@property(strong, nonatomic) PMTCoreDataPhoto *currentPhoto;
@property(strong, nonatomic) UIImage *currentImage;
@property(strong, nonatomic) NSMutableArray *pendingInvites;

@property(assign, nonatomic) CLLocationCoordinate2D lastLongTapLocation;

@property(nonatomic) CLLocationManager *locationManager;

@property(weak, nonatomic) IBOutlet UIButton *locationButton;
@property(weak, nonatomic) IBOutlet OCMapView *mapView;

@property(weak, nonatomic) NSArray *clusterAnnotationPhotos;

@end

@implementation PMTPhotoMapViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [self.navigationController.navigationBar setPhotoPreviewStyle];
    [self setupMapView];

    //Center map on user
    [self locationButtonTapped:nil];

    [self startLocationManager];

    [self resetLastPhotoData];

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController setNavigationBarHidden:YES];

    [self receivePhotos];
    self.clusterAnnotationPhotos = nil;
    [self updateAnnotations];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Public Accessors

- (PMTCoreDataPhoto *)currentPhoto {
    if (!_currentPhoto) {
        _currentPhoto = [[PMTCoreDataStorage sharedInstance] generatePhotoWithImage:self.currentImage];
    }
    return _currentPhoto;
}

- (NSMutableArray *)pendingInvites {
    if (!_pendingInvites) _pendingInvites = [NSMutableArray new];
    return _pendingInvites;
}

#pragma mark - Actions

- (IBAction)categoriesButtonTapped:(id)sender {
    [self.tabBarController.tabBar setHidden:YES];

    PMTPhotoCategoriesViewController *categoriesViewController = [[PMTPhotoCategoriesViewController alloc] initWithNibName:PMTCategoriesXibName bundle:[NSBundle mainBundle]];
    [categoriesViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];

    __weak PMTPhotoMapViewController *weakSelf = self;
    categoriesViewController.onViewControllerDismiss = ^() {
        [self receivePhotos];
        [weakSelf updateAnnotations];
        [weakSelf.tabBarController.tabBar setHidden:NO];
    };

    [self presentViewController:categoriesViewController animated:YES completion:nil];
}

- (IBAction)cameraButtonTapped:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    __weak PMTPhotoMapViewController *weakSelf = self;
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a Picture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf makePhoto];
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose From Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [weakSelf choosePhotoFromLibrary];
    }]];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [weakSelf dismissViewControllerAnimated:YES completion:^{
        }];
    }]];

    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)locationButtonTapped:(id)sender {
    self.mapView.userTrackingMode = MKUserTrackingModeFollow;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:NO completion:NULL];

    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];

    self.currentImage = chosenImage;

    [self presentPhotoPopupAnimated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - PMTPhotoMapPopupDelegate

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup tapPhotoPreviewForPhoto:(PMTCoreDataPhoto *)photo withImage:(UIImage *)image {
    //Photo is the same, but for clarity assign
    self.currentPhoto = photo;

    [popup dismissViewControllerAnimated:NO completion:nil];

    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:YES];

    PMTFullPhotoPreviewViewController *photoPreview = [[PMTFullPhotoPreviewViewController alloc] initWithNibName:PMTFullPhotoPreviewXibName bundle:[NSBundle mainBundle]];
    [photoPreview setAutomaticallyAdjustsScrollViewInsets:NO];

    [photoPreview setupWithPhoto:photo image:image];

    __weak PMTPhotoMapViewController *weakSelf = self;
    photoPreview.onViewControllerDismiss = ^() {
        [weakSelf presentPhotoPopupAnimated:YES];
    };

    [self.navigationController pushViewController:photoPreview animated:YES];
}

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup tapInviteForPhoto:(PMTCoreDataPhoto *)photo withImage:(UIImage *)image {
    self.currentPhoto = photo;

    [popup dismissViewControllerAnimated:NO completion:nil];

    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:YES];

    PMTInviteFriendsViewController *popupViewController = [[PMTInviteFriendsViewController alloc] initWithNibName:PMTInviteFriendsXibName bundle:[NSBundle mainBundle]];

    __weak PMTPhotoMapViewController *weakSelf = self;
    popupViewController.onInviteDoneTapped = ^(NSArray *selectedContacts) {
        if ([selectedContacts count]) {
            [weakSelf.pendingInvites addObject:selectedContacts];
        }

        [weakSelf dismissViewControllerAnimated:NO completion:nil];
        [weakSelf presentPhotoPopupAnimated:YES];
    };

    [self presentViewController:popupViewController animated:YES completion:nil];
}

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup didTapCancelButtonForPhoto:(PMTCoreDataPhoto *)photo {
    [popup dismissViewControllerAnimated:NO completion:nil];

    //if photo is newly created
    if (![self.currentPhoto updatedAt]) {
        [[PMTCoreDataStorage sharedInstance] deletePhoto:self.currentPhoto];
    }
    [self resetLastPhotoData];
}

- (void)photoMapPopup:(PMTPhotoMapPopupViewController *)popup didTapDoneButtonForPhoto:(PMTCoreDataPhoto *)photo {
    [popup dismissViewControllerAnimated:NO completion:nil];

    bool userLongTappedMap = CLLocationCoordinate2DIsValid(self.lastLongTapLocation);
    bool oldPhoto = photo.updatedAt != nil;

    if (!oldPhoto) {
        if (userLongTappedMap) {
            //Long tapped location
            photo.location = self.lastLongTapLocation;
        } else {
            //Current Location
            CLLocationCoordinate2D location = [[self.locationManager location] coordinate];
            photo.location = location;
        }
        [self addAnnotationForPhoto:photo];
    } else {
        [self updateAnnotations];
    }

    photo.updatedAt = [NSDate new];
    [[PMTCoreDataStorage sharedInstance] saveContext];

    [[PMTOperationQueue sharedInstance] uploadInvitesForCoreDataPhoto:photo invites:self.pendingInvites];

    [self resetLastPhotoData];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    if ([annotation isKindOfClass:[OCAnnotation class]]) {
        return [self clusterViewForAnnotation:mapView annotation:annotation];
    }
    return [self photoViewForAnnotation:mapView annotation:annotation];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view.annotation isKindOfClass:[PMTPhotoMapAnnotation class]]) {
        [self.mapView setCenterCoordinate:view.annotation.coordinate animated:YES];

        PMTPhotoMapAnnotation *annotation = (PMTPhotoMapAnnotation *) view.annotation;
        [annotation showCallout:view];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if (![view.annotation isKindOfClass:[PMTPhotoMapAnnotation class]]) {
        return;
    }

    PMTPhotoMapAnnotation *annotation = (PMTPhotoMapAnnotation *) view.annotation;
    [annotation hideCallout];
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated {
    [self.mapView doClustering];
}

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated {
    NSString *buttonIconName = mode == MKUserTrackingModeFollow ? @"IconMap" : @"IconCenterOnMe";
    [self.locationButton setImage:[UIImage imageNamed:buttonIconName] forState:UIControlStateNormal];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    if ([view.annotation isKindOfClass:[OCAnnotation class]]) {

        [self.navigationController setNavigationBarHidden:NO];
        [self.tabBarController.tabBar setHidden:NO];

        OCAnnotation *annotationCluster = view.annotation;
        NSArray *annotations = annotationCluster.annotationsInCluster;
        NSMutableArray *photoFromAnnotations = [NSMutableArray new];

        for (PMTPhotoMapAnnotation *photoAnnotation in annotations) {
            [photoFromAnnotations addObject:photoAnnotation.photo];
        }

        self.clusterAnnotationPhotos = photoFromAnnotations;

        [self performSegueWithIdentifier:PMTFromAnnotationToTimelineSegue sender:self];
    }
}

#pragma mark - GestureRecognizer

- (void)longTap:(UIGestureRecognizer *)sender {
    if (sender.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint touchPoint = [sender locationInView:self.mapView];
    CLLocationCoordinate2D location =
            [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];

    self.lastLongTapLocation = location;

    [self cameraButtonTapped:nil];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[PMTTimelineTableViewController class]]) {
        PMTDataSourceTimelineAnnotations *dataSource = [PMTDataSourceTimelineAnnotations new];
        dataSource.photoStorage = [[PMTPhotoStorage alloc] initWithPhotoArray:self.clusterAnnotationPhotos];

        PMTTimelineTableViewController *timelineTableViewController = (PMTTimelineTableViewController *) segue.destinationViewController;

        timelineTableViewController.shouldBeInNavigationController = YES;
        timelineTableViewController.photoStorage = dataSource;
    }
    [super prepareForSegue:segue sender:sender];
}

#pragma mark - Private

- (void)presentPhotoPopupAnimated:(BOOL)animated {
    PMTPhotoMapPopupViewController *popupViewController = [[PMTPhotoMapPopupViewController alloc] initWithNibName:PMTPhotoMapPopupXibName bundle:[NSBundle mainBundle]];

    [popupViewController setModalPresentationStyle:UIModalPresentationOverFullScreen];
    popupViewController.popupDelegate = self;

    [popupViewController initWithPhoto:self.currentPhoto image:self.currentImage];

    [self presentViewController:popupViewController animated:animated completion:nil];
}

- (void)makePhoto {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Error" withText:@"Device has no camera"];
    } else {
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)resetLastPhotoData {
    self.pendingInvites = nil;

    self.currentPhoto = nil;
    self.currentImage = nil;

    self.lastLongTapLocation = kCLLocationCoordinate2DInvalid;
}

- (void)updateAnnotations {
    NSArray *annotations = self.mapView.annotations;

    NSPredicate *predicateByCategory = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        PMTCoreDataPhoto *photo = ((PMTPhotoMapAnnotation *) evaluatedObject).photo;
        PMTPhotoCategory *photoCategory = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) photo.category.intValue];
        return !photoCategory.isActive;
    }];

    [self.mapView removeAnnotations:annotations];
    [self.mapView addAnnotations:annotations];

    self.mapView.annotationsToIgnore = [NSMutableSet setWithArray:[annotations filteredArrayUsingPredicate:predicateByCategory]];
    [self.mapView doClustering];
}

- (void)choosePhotoFromLibrary {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)setupMapView {
    self.mapView.delegate = self;

    self.mapView.clusterByGroupTag = NO;
    self.mapView.clusterSize = 0.2f;
    self.mapView.clusterInvisibleViews = YES;

    self.mapView.rotateEnabled = NO;
    [self addGestureRecognizerToMapView];
}

- (void)addGestureRecognizerToMapView {
    UILongPressGestureRecognizer *longTapGestureRecognizer = [[UILongPressGestureRecognizer alloc]
            initWithTarget:self action:@selector(longTap:)];
    longTapGestureRecognizer.minimumPressDuration = 2;
    [self.mapView addGestureRecognizer:longTapGestureRecognizer];
}

- (void)startLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    [self.locationManager startUpdatingLocation];
    [self.locationManager requestWhenInUseAuthorization]; // Add This Line
}

- (void)addAnnotationForPhoto:(PMTCoreDataPhoto *)photo {
    PMTPhotoMapAnnotation *annotation = [PMTPhotoMapAnnotation new];
    annotation.photo = photo;
    [self.mapView addAnnotation:annotation];
}

- (MKAnnotationView *)clusterViewForAnnotation:(MKMapView *)mapView annotation:(id <MKAnnotation>)annotation {
    PMTPhotoMapClusterAnnotation *annotationView;
    OCAnnotation *clusterAnnotation = (OCAnnotation *) annotation;

    annotationView = (PMTPhotoMapClusterAnnotation *) [mapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
    if (!annotationView) {
        annotationView = [[PMTPhotoMapClusterAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
        annotationView.canShowCallout = YES;
        annotationView.centerOffset = CGPointMake(0, -20);

        UIButton *myDetailAccessoryButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        myDetailAccessoryButton.frame = CGRectMake(0, 0, 23, 23);
        myDetailAccessoryButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        myDetailAccessoryButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        annotationView.rightCalloutAccessoryView = myDetailAccessoryButton;

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 20)];
        [label setBackgroundColor:[UIColor redColor]];
        label.font = [label.font fontWithSize:10];
        annotationView.numberOfPhotosLabel = label;

        [annotationView addSubview:label];
    }

    clusterAnnotation.title = @"Cluster";
    clusterAnnotation.subtitle = [NSString stringWithFormat:@"Containing annotations: %lu", (unsigned long) [clusterAnnotation.annotationsInCluster count]];

    annotationView.numberOfPhotosLabel.text = [NSString stringWithFormat:@"N : %lu", (unsigned long) [clusterAnnotation.annotationsInCluster count]];

    annotationView.image = [UIImage imageNamed:@"IconMap"];

    return annotationView;
}

- (MKAnnotationView *)photoViewForAnnotation:(MKMapView *)mapView annotation:(id <MKAnnotation>)annotation {
    PMTPhotoMapAnnotationCalloutView *annotationView = (PMTPhotoMapAnnotationCalloutView *) [mapView dequeueReusableAnnotationViewWithIdentifier:@"Annonation"];

    if (annotationView) {
        annotationView.annotation = annotation;
    } else {
        annotationView = [[PMTPhotoMapAnnotationCalloutView alloc] initWithAnnotation:annotation
                                                                      reuseIdentifier:@"Annonation"];
    }

    __weak PMTPhotoMapViewController *weakSelf = self;
    ((PMTPhotoMapAnnotation *) annotation).onPhotoAnnotationCalloutTapped = ^(PMTCoreDataPhoto *senderPhoto) {
        weakSelf.currentPhoto = senderPhoto;
        [weakSelf presentPhotoPopupAnimated:YES];
    };

    annotationView.canShowCallout = NO;

    PMTCoreDataPhoto *annotationForPhoto = ((PMTPhotoMapAnnotation *) annotation).photo;
    PMTPhotoCategory *photoCategory = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) annotationForPhoto.category.intValue];
    annotationView.image = [UIImage imageNamed:photoCategory.mapImage];

    [annotationView setHidden:!photoCategory.isActive];

    return annotationView;
}

- (void)receivePhotos {
    PMTCoreDataUser *user = [[PMTCoreDataStorage sharedInstance] currentUser];
    [self.mapView removeAnnotations:[self.mapView annotations]];
    [user.photos enumerateObjectsUsingBlock:^(PMTCoreDataPhoto *photo, BOOL *stop) {
        [self addAnnotationForPhoto:photo];
    }];
}

@end