//
//  PMTFullPhotoPreviewViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/23/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTFullPhotoPreviewViewController.h"
#import "PMTFullPhotoPreviewFooterView.h"
#import "PMTConstants.h"
#import "PMTCoreDataPhoto.h"

@interface PMTFullPhotoPreviewViewController ()

@property(strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property(strong, nonatomic) UIImageView *imageView;

@property(strong, nonatomic) PMTFullPhotoPreviewFooterView *footerView;

@property(strong, nonatomic) PMTCoreDataPhoto *previewPhoto;
@property(strong, nonatomic) UIImage *image;

@end

@implementation PMTFullPhotoPreviewViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [self addGestureRecognizers];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateUI];
}

- (void)viewDidDisappear:(BOOL)animated {
    self.onViewControllerDismiss();
    [super viewDidDisappear:YES];
}

#pragma mark - Custom Accessors

- (PMTFullPhotoPreviewFooterView *)footerView {
    if (!_footerView) {
        int footerHeight = 100;
        CGRect footerFrame = CGRectMake(0, self.view.bounds.size.height - footerHeight, self.view.bounds.size.width, footerHeight);

        PMTFullPhotoPreviewFooterView *footer = [[[NSBundle mainBundle] loadNibNamed:PMTPhotoMapPreviewFooterXibName owner:self options:nil] firstObject];

        footer.frame = footerFrame;
        [self.view addSubview:footer];
        [self.view bringSubviewToFront:footer];

        _footerView = footer;
    }
    return _footerView;
}

#pragma mark - Public

- (void)setupWithPhoto:(PMTCoreDataPhoto *)photo image:(UIImage *)image {
    self.previewPhoto = photo;
    self.image = image;
}

//ToDo : Think, how to differ single touch and double touch
- (void)addGestureRecognizers {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]
            initWithTarget:self action:@selector(singleTap:)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [tapGestureRecognizer setNumberOfTouchesRequired:1];
    [self.scrollView addGestureRecognizer:tapGestureRecognizer];

    UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]
            initWithTarget:self action:@selector(doubleTap:)];
    [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
    [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
    [self.scrollView addGestureRecognizer:doubleTapGestureRecognizer];
}

#pragma mark - UITapGestureRecongizers

- (void)singleTap:(UIGestureRecognizer *)sender {
    BOOL isNavigationBarHidden = self.navigationController.isNavigationBarHidden;
    [self.navigationController setNavigationBarHidden:!isNavigationBarHidden animated:YES];

    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        float footerVerticalPosition = isNavigationBarHidden ? 0 : 100;
        [self.footerView setTransform:CGAffineTransformMakeTranslation(0, footerVerticalPosition)];
    }                completion:nil];
}

- (void)doubleTap:(UIGestureRecognizer *)sender {
    if (self.scrollView.zoomScale > self.scrollView.minimumZoomScale)
        [self.scrollView setZoomScale:self.scrollView.minimumZoomScale animated:YES];
    else
        [self.scrollView setZoomScale:self.scrollView.maximumZoomScale animated:YES];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat offsetX = (CGFloat) MAX((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = (CGFloat) MAX((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0);

    self.imageView.center = CGPointMake((CGFloat) (scrollView.contentSize.width * 0.5 + offsetX),
            (CGFloat) (scrollView.contentSize.height * 0.5 + offsetY));
}

#pragma mark - Private

- (void)updateUI {
    [self updateImageView];
    [self updateScrollView];
    [self updateFooterLabels];
}

- (void)updateScrollView {
    self.view.frame = [[UIScreen mainScreen] bounds];
    self.scrollView.frame = [[UIScreen mainScreen] bounds];

    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 5.0;

    self.scrollView.zoomScale = 1.0;
    [self.scrollView addSubview:self.imageView];

    self.scrollView.contentSize = self.image.size;

    self.imageView.frame = self.scrollView.bounds;
}

- (void)updateImageView {
    self.imageView = [[UIImageView alloc] initWithImage:self.image];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)updateFooterLabels {
    [self.footerView updateWithPhoto:self.previewPhoto];
}

@end
