//
// Created by Nikolay Dmitriev on 11/19/15.
// Copyright (c) 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTSignInViewController.h"
#import "PMTSignUpViewController.h"
#import "Parse/Parse.h"
#import "PMTViewUtilities.h"
#import "PMTAppDelegate.h"
#import "PMTConstants.h"

@interface PMTSignInViewController ()

@property(weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property(weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation PMTSignInViewController

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:animated];
}

#pragma mark - Actions

- (IBAction)signInButtonTapped:(id)sender {
    [self sendSignInRequest];
}

- (IBAction)signUpButtonTapped:(id)sender {
    UIViewController *signUpController = [[PMTSignUpViewController alloc] initWithNibName:PMTSignUpXibName bundle:[NSBundle mainBundle]];

    [self.navigationController pushViewController:signUpController animated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Private

- (void)sendSignInRequest {
    NSString *userName = self.usernameTextField.text;
    NSString *password = self.passwordTextField.text;

    [[PMTViewUtilities sharedInstance] showLoadingView:self.view];

    __weak id weakSelf = self;
    [PFUser logInWithUsernameInBackground:userName password:password
                                    block:^(PFUser *user, NSError *error) {
                                        [[PMTViewUtilities sharedInstance] dismissLoadingView];
                                        if (user) {
                                            [weakSelf onSuccessfulLogin];
                                        } else {
                                            [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Error"
                                                                                             withText:error.description];
                                        }
                                    }];
}

- (void)onSuccessfulLogin {
    PMTAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate showApplicationView];
}

@end