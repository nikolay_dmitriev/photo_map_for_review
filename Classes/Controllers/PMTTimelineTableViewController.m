//
//  PMTTimelineTableViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTTimelineTableViewController.h"
#import "PMTTimelineTableViewCell.h"
#import "PMTConstants.h"
#import "PMTFullPhotoPreviewViewController.h"
#import "PMTPhotoCategoriesViewController.h"
#import "UINavigationBar+PMTUINavigationBarCategory.h"
#import "PMTPhotoStorage.h"
#import "PMTDataSourceTimelineParse.h"
#import "PMTCoreDataPhoto.h"
#import "PFImageView.h"
#import "PMTPhotoImageView.h"
#import "PMTViewUtilities.h"
#import "PMTDataSourceTimelineParse.h"

@interface PMTTimelineTableViewController ()

@property(strong, nonatomic) UISearchController *searchController;

@property(weak, nonatomic) IBOutlet UIView *placeholderViewForSearch;
@property(weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end

@implementation PMTTimelineTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [self.tableView registerNib:[UINib nibWithNibName:PMTTimelineTableViewCellXibName bundle:nil] forCellReuseIdentifier:@"TimelineTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:PMTTimelineTableViewCellExtendedXibName bundle:nil] forCellReuseIdentifier:@"TimelineTableViewCellExtended"];

    [self.navigationController.navigationBar setPhotoPreviewStyle];

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.refreshControl endRefreshing];
    [self updateNavigationAndTabBar];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [self createSearchController];
}

- (void)dealloc{
    self.photoStorage = nil;
    [self.searchController.view removeFromSuperview];
}

#pragma mark - Custom Accessors

- (PMTPhotoStorage *)activeStorage {
    if (self.searchController.active) {
        return self.photoStorage.photoStorageFilteredByCategoriesAndSearch;
    }
    return self.photoStorage.photoStorageFilteredByCategories;
}

- (UIRefreshControl *)refreshControl {
    if(!_refreshControl){
        _refreshControl = [UIRefreshControl new];
        [self.tableView addSubview:_refreshControl];

        [_refreshControl addTarget:self
                           action:@selector(fetchNewPhotos)
                 forControlEvents:UIControlEventValueChanged];
    }
    return _refreshControl;
}

- (PMTDataSourceTimelineParse *)photoStorage {
    if (!_photoStorage) {
        _photoStorage = [PMTDataSourceTimelineParse new];
    }
    return _photoStorage;
}

#pragma mark - Actions

- (IBAction)categoriesButtonTapped:(id)sender {
    PMTPhotoCategoriesViewController *categoriesViewController = [[PMTPhotoCategoriesViewController alloc] initWithNibName:PMTCategoriesXibName bundle:[NSBundle mainBundle]];
    [categoriesViewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];

    [self presentViewController:categoriesViewController animated:YES completion:nil];

    __weak PMTTimelineTableViewController *weakSelf = self;
    categoriesViewController.onViewControllerDismiss = ^{
        [weakSelf updateNavigationAndTabBar];
        [weakSelf filterContentForUpdatedCategories];
    };

    [self.navigationController setNavigationBarHidden:YES];
    [self.tabBarController.tabBar setHidden:YES];
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.activeStorage totalMonthInStorage];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.activeStorage photosForMonthWithIndex:(NSUInteger) section] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    PMTCoreDataPhoto *photo = [[self.activeStorage photosForMonthWithIndex:(NSUInteger) section] firstObject];

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setDateFormat:@"MMMM yyyy"];
    return [dateFormatter stringFromDate:photo.date];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PMTCoreDataPhoto *photo = [self.activeStorage photosForMonthWithIndex:(NSUInteger) indexPath.section][(NSUInteger) indexPath.row];

    id cell;

    if ([photo.photoDescription length] == 0) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"TimelineTableViewCell" forIndexPath:indexPath];
    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"TimelineTableViewCellExtended" forIndexPath:indexPath];
    }

    [cell updateWithPhoto:photo];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:YES];

    PMTFullPhotoPreviewViewController *photoPreview = [[PMTFullPhotoPreviewViewController alloc] initWithNibName:PMTFullPhotoPreviewXibName bundle:[NSBundle mainBundle]];
    [photoPreview setAutomaticallyAdjustsScrollViewInsets:NO];
    photoPreview.onViewControllerDismiss = ^() {
    };

    PMTCoreDataPhoto *photo = [self.activeStorage photosForMonthWithIndex:(NSUInteger) indexPath.section][(NSUInteger) indexPath.row];
    PMTTimelineTableViewCell *cell = (PMTTimelineTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];

    if (!cell.photoImageView.image) {
        return; // Don't allow to tap on photos, which are still loading
    }

    [photoPreview setupWithPhoto:photo image:cell.photoImageView.image];

    [self.navigationController pushViewController:photoPreview animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Don't fetch new data when search
    if (self.searchController.active) {
        return;
    }

    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height) {
        __weak PMTTimelineTableViewController *weakSelf = self;
        [self.photoStorage receivePhotosWithBlock:^{
            [weakSelf.refreshControl endRefreshing];
            [weakSelf.tableView reloadData];
        }];
    }
}

#pragma mark - UISearchControllerDelegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;

    if (!searchString.length) {
        return;
    }

    bool stringStartsWithHashTag = [[searchString substringToIndex:1] isEqualToString:@"#"];
    if (!stringStartsWithHashTag) {
        //Adding # to find hashTags only
        searchString = [@"#" stringByAppendingString:searchString];
    }

    [self filterContentForSearchString:searchString];
    [self.tableView reloadData];
}

#pragma mark - Private

- (void)fetchNewPhotos {
    self.photoStorage = nil;
    [self.tableView reloadData];
}

- (void)createSearchController {
    if(self.searchController){
        return;
    }

    UISearchController *uiSearchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController = uiSearchController;
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.searchController.searchBar.delegate = self;

    [self.placeholderViewForSearch addSubview:self.searchController.searchBar];
    [self.searchController.searchBar sizeToFit];
}

- (void)filterContentForUpdatedCategories {
    [self.photoStorage filterWithActiveCategories];
    [self.tableView reloadData];
}

- (void)filterContentForSearchString:(NSString *)searchString {
    [self.photoStorage filterWithSearchString:searchString];
}

- (void)updateNavigationAndTabBar {
    if (!self.shouldBeInNavigationController) {
        [self.navigationController setNavigationBarHidden:YES];
        [self.tabBarController.tabBar setHidden:NO];
    }else{
        [self.navigationController setNavigationBarHidden:NO];
        [self.tabBarController.tabBar setHidden:YES];
    }
}


@end
