//
//  PMTPhotoCategoriesViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTPhotoCategoriesViewController.h"
#import "PMTPhotoCategories.h"
#import "PMTPhotoCategory.h"
#import "PMTPhotoCategoryTableViewCell.h"
#import "PMTConstants.h"

@interface PMTPhotoCategoriesViewController()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PMTPhotoCategoriesViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {

    [self.tableView registerNib:[UINib nibWithNibName:PMTPPhotoCategoryTableViewCellXibName bundle:nil] forCellReuseIdentifier:@"CategoryTableViewCell"];

    //hide line between cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)doneButtonTapped:(id)sender {
    if(self.onViewControllerDismiss){
        self.onViewControllerDismiss();
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [PMTPhotoCategories sharedInstance].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PMTPhotoCategoryTableViewCell *cell = (PMTPhotoCategoryTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CategoryTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    PMTPhotoCategory *category = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) indexPath.row];

    //Handles tap on status button in cell
    cell.onStatusButtonTap = ^(PMTPhotoCategoryTableViewCell *sender) {
        NSIndexPath *senderCellIndexPath = [self.tableView indexPathForCell:sender];
        PMTPhotoCategory *senderCellCategory = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) senderCellIndexPath.row];
        senderCellCategory.active = !senderCellCategory.active;
        [sender setCellStyleForCategory:category];
    };

    [cell setCellStyleForCategory:category];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self changeStatusForCategoryWithIndexPath:indexPath];
}

#pragma mark - Private

- (void)changeStatusForCategoryWithIndexPath:(NSIndexPath *)indexPath {
    PMTPhotoCategory *category = [[PMTPhotoCategories sharedInstance] categoryForType:(PhotoCategoriesTypes) indexPath.row];
    category.active = !category.active;

    [self.tableView reloadData];
}

@end
