//
//  PMTInviteFriendsViewController.h
//  PhotoMap
//
//  Created by mac-184 on 1/12/16.
//  Copyright © 2016 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
@import ContactsUI;

typedef void (^InviteFriendsDismissBlock)(NSArray *selectedContacts);

@interface PMTInviteFriendsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(copy, nonatomic) InviteFriendsDismissBlock onInviteDoneTapped;

@end
