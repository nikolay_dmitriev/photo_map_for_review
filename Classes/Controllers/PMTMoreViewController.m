//
//  PMTMoreViewController.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/21/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTMoreViewController.h"
#import "PMTAppDelegate.h"
#import "Parse/Parse.h"
#import "PMTCoreDataParseSynchronization.h"
#import "PMTViewUtilities.h"
#import "PMTOperationQueue.h"

@interface PMTMoreViewController ()

@property(strong, nonatomic) PMTCoreDataParseSynchronization *sync;

@end

@implementation PMTMoreViewController

#pragma mark - Actions

- (void)viewDidLoad {
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Custom Accessors

- (PMTCoreDataParseSynchronization *)sync {
    if (!_sync) _sync = [PMTCoreDataParseSynchronization new];
    return _sync;
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (indexPath.row == 0) {
        cell.textLabel.text = @"Logout";
    } else {
        cell.textLabel.text = @"Sync";
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [PFUser logOut];
        PMTAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate showLoginView];
    } else {
        if(![PMTOperationQueue sharedInstance].isEmpty){
            [[PMTViewUtilities sharedInstance] showAlertViewWithTitle:@"Please Wait" withText:@"Can't synchronize now"];
            return;
        }

        [self.sync synchronize];

        __weak PMTMoreViewController *weakSelf = self;
        self.sync.onCoreDataSynchronizationCompleted = ^() {
            [[PMTViewUtilities sharedInstance] dismissLoadingView];
            [weakSelf.tabBarController.tabBar setHidden:NO];
        };

        [[PMTViewUtilities sharedInstance] showLoadingView:self.view];
        [self.tabBarController.tabBar setHidden:YES];
    }
}

@end
