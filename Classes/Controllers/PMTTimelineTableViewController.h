//
//  PMTTimelineTableViewController.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/22/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMTDataSourceTimelineParse;

@interface PMTTimelineTableViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchBarDelegate>

@property (assign, nonatomic) BOOL shouldBeInNavigationController;
@property (strong, nonatomic) PMTDataSourceTimelineParse *photoStorage;

@end
