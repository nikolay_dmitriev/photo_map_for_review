//
//  PMTUIColorCategory.h
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/26/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PMTUIColorCategory)

+ (UIColor *)colorWithHex:(NSString *)hexCode;

+ (UIImage *)imageWithColor:(UIColor *)color alpha:(float)alpha;

@end
