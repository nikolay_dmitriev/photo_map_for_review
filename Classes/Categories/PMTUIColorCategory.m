//
//  PMTUIColorCategory.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/26/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import "PMTUIColorCategory.h"

@implementation UIColor (PMTUIColorCategory)

+ (UIColor *)colorWithHex:(NSString *)hexCode {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexCode];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];

    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0 green:((rgbValue & 0xFF00) >> 8) / 255.0 blue:(rgbValue & 0xFF) / 255.0 alpha:1.0];
}

+ (UIImage *)imageWithColor:(UIColor *)color alpha:(float)alpha {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);

    UIGraphicsBeginImageContext(rect.size);

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextSetAlpha(context, alpha);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return image;
}

@end