//
//  main.m
//  PhotoMap
//
//  Created by Nikolay Dmitriev on 11/17/15.
//  Copyright © 2015 Nikolay Dmitriev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMTAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PMTAppDelegate class]));
    }
}
